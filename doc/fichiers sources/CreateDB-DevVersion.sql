#===========================================================#
# HE-Arc Ingenierie         -         Filiere informatique  #
# 2ème année (2021-2022)    -       Développement logiciel  #
#                                               (ISC2il-a)  #
#                                                           #
# Projet    -    Application Web II    -    site "Arquizz"  #
# Auteurs :            Allemann Jonas & Chappuis Sebastien  #
#                                               13.03.2022  #
#                                                           #
# Fichier contenant du script :                      MySql  #
# Requetes sur le schéma (base de données) :       Arquizz  #
# Création des tables générée à partir du MCD réalisé avec  #
#                                                  JMerise  #
#===========================================================#

#===========================================================#
# /!\ REMARQUES VERSION DEVELOPPEMENT /!\                   #
#                                                           #
# Les events se déclanches toutes les 30 secondes,          #
# A changer la période pour la DB de production             #
# 	(ex toute les heures)                               #
#                                                           #
# Il y a également des insertions dans le but d effectuer   #
# des tests manuels (pour l auto-suppression des event)     #
#===========================================================#

#============================================================
# Configuration
#============================================================

SET NAMES utf8;
SET GLOBAL event_scheduler="ON";

#============================================================
# Création de la base de donnée
# Remarque : "SCHEMA" est un synonyme de "DATABASE" en MySql
#============================================================

DROP SCHEMA IF EXISTS arquizz;

CREATE SCHEMA arquizz;
USE arquizz;

#============================================================
# Création des tables
#============================================================

#------------------------------------------------------------
#    Table: User
#------------------------------------------------------------

CREATE TABLE User(
        id_user  Int  Auto_increment  NOT NULL ,
        email    Varchar (350) NOT NULL ,
        password Varchar (1000) NOT NULL
	,CONSTRAINT User_AK UNIQUE (email)
	,CONSTRAINT User_PK PRIMARY KEY (id_user)
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;


#------------------------------------------------------------
#    Table: Quizz
#------------------------------------------------------------

CREATE TABLE Quizz(
        id_quizz Int  Auto_increment  NOT NULL ,
        name     Varchar (20) NOT NULL ,
        deletion Bool NOT NULL DEFAULT FALSE,
        id_user  Int NOT NULL
	,CONSTRAINT Quizz_PK PRIMARY KEY (id_quizz)

	,CONSTRAINT Quizz_User_FK FOREIGN KEY (id_user) REFERENCES User(id_user)
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;


#------------------------------------------------------------
#    Table: Room
#------------------------------------------------------------

CREATE TABLE Room(
        id_room          Int  Auto_increment  NOT NULL ,
        code             Varchar (15) NOT NULL ,
        question_time    Int NOT NULL ,
        auto_kill        TimeStamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
        state            Enum ("Question","Leaderboard","Finished","Waiting","None") NOT NULL ,
        current_question Int NOT NULL ,
        time_next_state  TimeStamp NOT NULL ,
        id_quizz         Int NOT NULL ,
        id_user          Int NOT NULL
	,CONSTRAINT Room_AK UNIQUE (code)
        ,CONSTRAINT Room_PK PRIMARY KEY (id_room)

	,CONSTRAINT Room_Quizz_FK FOREIGN KEY (id_quizz) REFERENCES Quizz(id_quizz)
	,CONSTRAINT Room_User0_FK FOREIGN KEY (id_user) REFERENCES User(id_user)
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;


#------------------------------------------------------------
#    Table: Question
#------------------------------------------------------------

CREATE TABLE Question(
        id_question Int  Auto_increment  NOT NULL ,
        sentence    Varchar (100) NOT NULL ,
        priority    Int NOT NULL ,
        id_quizz    Int NOT NULL
	,CONSTRAINT Question_PK PRIMARY KEY (id_question)

	,CONSTRAINT Question_Quizz_FK FOREIGN KEY (id_quizz) REFERENCES Quizz(id_quizz)  ON DELETE CASCADE
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;


#------------------------------------------------------------
#    Table: Answer
#------------------------------------------------------------

CREATE TABLE Answer(
        id_answer   Int  Auto_increment  NOT NULL ,
        sentence    Varchar (100) NOT NULL ,
        is_correct  Bool NOT NULL ,
        type        Enum ("None","A","B","C","D") NOT NULL ,
        id_question Int NOT NULL
	,CONSTRAINT Answer_PK PRIMARY KEY (id_answer)

	,CONSTRAINT Answer_Question_FK FOREIGN KEY (id_question) REFERENCES Question(id_question)  ON DELETE CASCADE
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;


#------------------------------------------------------------
#    Table: Player
#------------------------------------------------------------

CREATE TABLE Player(
        id_player      Int  Auto_increment  NOT NULL ,
        name           Varchar (20) NOT NULL ,
        score          Int NOT NULL ,
        current_answer Enum ("None","A","B","C","D") NOT NULL DEFAULT "None",
        id_room        Int NOT NULL
	,CONSTRAINT Player_PK PRIMARY KEY (id_player)

	,CONSTRAINT Player_Room_FK FOREIGN KEY (id_room) REFERENCES Room(id_room) ON DELETE CASCADE
)ENGINE=InnoDB CHARSET=utf8 ROW_FORMAT=DYNAMIC;










#============================================================
# Création des events
#============================================================
# Event vs Trigger : https://www.oreilly.com/library/view/mysql-administrators-bible/9780470416914/ch07.html#:~:text=A%20trigger%20is%20invoked%20automatically,occurrence%20or%20a%20regular%20occurrence.


#========================================
#    Pour : Suppression de salon
#========================================

CREATE EVENT delete_room_event
	ON SCHEDULE EVERY 30 SECOND 
	ON COMPLETION PRESERVE
	ENABLE
DO
	DELETE FROM Room WHERE NOW() > auto_kill + INTERVAL 1 DAY;

#========================================
#    Pour : Suppression de quizz
#========================================

CREATE EVENT delete_quizz_event
	ON SCHEDULE EVERY 30 SECOND 
	ON COMPLETION PRESERVE
	ENABLE
DO
        DELETE FROM Quizz 
        WHERE deletion = TRUE 
                AND Quizz.id_quizz NOT IN (SELECT Room.id_quizz FROM Room);


#========================================
#    Pour : Mise à jour de l etat du salon
#========================================

DELIMITER $$

CREATE EVENT autoupdate_room_state
	ON SCHEDULE EVERY 15 SECOND 
	ON COMPLETION PRESERVE
	ENABLE
DO BEGIN
        -- MySql require declare inside begin-end, not inside if
        DECLARE nextState Enum ("Question","Leaderboard","Finished","Waiting","None");


        UPDATE Room
                SET     state = "Finished"
                WHERE state = "Question"
                        AND time_next_state <= NOW() 
                        AND current_question >= (SELECT COUNT(*) FROM Question WHERE Question.id_quizz = Room.id_quizz);

        UPDATE Room
                SET     state = "Leaderboard",
                        time_next_state = TIMESTAMPADD(SECOND, question_time, NOW())
                WHERE state = "Question" AND time_next_state <= NOW();

        UPDATE Room
                SET     state = "Question",
                        time_next_state = TIMESTAMPADD(SECOND, question_time, NOW()),
                        current_question = current_question + 1
                WHERE state = "Leaderboard" AND time_next_state <= NOW();


        
END$$    

DELIMITER ;





#============================================================
# Insersions dans les tables
#============================================================

# Insertion de test dev

# password = 1234
INSERT INTO `User` (`id_user`, `email`, `password`) 
        VALUES (1, '1234@exemple.com', '$2y$10$wHoj.I2BLu71fXIJnOkFYejVnOC208x11byXY0hDcLuEI/1EMxLf2');

INSERT INTO `Quizz` (`id_quizz`, `name`, `deletion`, `id_user`)
        VALUES (2, 'MonQuizz', 0, 1);

INSERT INTO `Room` (`id_room`, `code`, `question_time`, `auto_kill`, `state`, `current_question`, `time_next_state`, `id_quizz`, `id_user`)
        VALUES (1, 'abc', 6, '2023-04-03 17:00:00', 'Question', 1, '2023-03-13 23:00:00', 2, 1);

INSERT INTO `Player` (`id_player`, `name`, `score`, `current_answer`, `id_room`)
        VALUES (1, 'HelloWorld', 0, 'None', 1);

INSERT INTO `Question` (`id_question`, `sentence`, `priority`, `id_quizz`)
        VALUES
                (1, 'quest1', 2, 2),
                (2, 'quest2', 1, 2);

INSERT INTO `Answer` (`id_answer`, `sentence`, `is_correct`, `type`, `id_question`)
        VALUES
                (1, 'Rep1', 0, 'A', 1),
                (2, 'Rep3', 0, 'C', 1),
                (3, 'Rep4', 0, 'D', 1),
                (4, 'Rep2', 1, 'B', 1),
                (5, 'Q2R4', 1, 'D', 2),
                (6, 'Q2R3', 0, 'C', 2),
                (7, 'Q2R2', 0, 'B', 2),
                (8, 'Q2R1', 0, 'A', 2);

# Insertion de test d auto-suppression
# Ces entrées doivent se supprimer toute seule

#  - Suppresson dans environ 5 min après la creation de la DB

#    - Ce quizz ne sera supprimer qu après la suppression du salon lié
INSERT INTO `Quizz` (`id_quizz`, `name`, `deletion`, `id_user`)
        VALUES (4, 'SeraSupp2', 0, 1);

INSERT INTO `Room` (`id_room`, `code`, `question_time`, `auto_kill`, `state`, `current_question`, `time_next_state`, `id_quizz`, `id_user`)
        VALUES (2, 'SeraSupp', 6, NOW() + INTERVAL 5 MINUTE - INTERVAL 1 DAY , 'Question', 1, '2023-03-13 23:00:00', 4, 1);

INSERT INTO `Player` (`id_player`, `name`, `score`, `current_answer`, `id_room`)
        VALUES (2, 'SeraSupp', 0, 'None', 2);

INSERT INTO `Player` (`id_player`, `name`, `score`, `current_answer`, `id_room`)
        VALUES (3, 'SeraSupp2', 0, 'None', 2);

UPDATE `Quizz` SET `deletion` = TRUE WHERE `id_quizz` = 4;

#  - Ce quizz sera supprimer lors du prochain déclanchement de l event
INSERT INTO `Quizz` (`id_quizz`, `name`, `deletion`, `id_user`)
        VALUES (3, 'SeraSupp', 1, 1);