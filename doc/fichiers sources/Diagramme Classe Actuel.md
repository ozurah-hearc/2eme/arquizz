# Diagramme de classes

## Diagramme de classe des contrôleurs

```mermaid
classDiagram
class IndexController {
  +index()
}

class AboutController {
  +index()
}

class ConnectionController {
  +index()
  +connect()
  +disconnect()
  +showCreateAccount()
  +createAccount()
  -keepConnectedUser(User)
}

class RoomController {
  <<Not fully implemented now>>
  +MIN_STATE_TIME : const int
  +index()
  +showJoinRoom()
  +joinRoom()
  +dataRoom()
  +answerQuestion()
  +showCreateRoom()
  +createRoom()
  +newRoom()
  +startRoom()
  +stopRoom()
  -roomStartStopGuardClose(actionName : string)
}

class QuizzController {
  +index()
  +showQuizz()
  +editQuizz()
  -onDeleteQuizz(Quizz)
  -onEditQuizz(Quizz, questionsId : int[] , deletedQuestionsId : int[])
}
```

## Diagramme de classe des modèles

```mermaid
classDiagram

%% "[]bool_int" means an array with 0 = bool, 1 = int

class Model {
  +static CLASS_NAME
  +static TABLE_NAME
  #abstract getId()
  +static getTableName() string
  +static getClassName() string
  +static getDb()
  +static fetch()
  +static fetchId(id)
  +static fetchAll()
  +create() []bool_int
  +update() bool
  +delete() bool
}

class User {
  #id_user : int
  #password : string
  #email : string
  #rooms: Room[]
  #quizzes: Quizz[]
  +getId() int
  +setId(int)
  +getPassword() string
  +setPassword(string)
  +setAndHashPassword(unhashedPassword : string)
  +getEmail() string
  +setEmail(string)
  +getRooms() Room[]
  +setRooms(Room[])
  +getQuizzes() Quizz[]
  +setQuizzes(Quizz[])
  +authenticate(password : string) bool
  +createAccount() bool
}

class Quizz {
  #id_quizz : int
  #name : string
  #deletion : bool
  #id_user : int
  #rooms : Room[]
  #questions : Question[]
  +getId() int
  +setId(int)
  +getName() string
  +setName(string)
  +getDeletion() bool
  +setDeletion(bool)
  +getIdUser() int
  +setIdUser(int)
  +getQuestions() Question[]
  +setQuestions(Question[])
  +getRooms() Room[]
  +setRooms(Room[])

  %% DB
  +save() bool
  +edit() bool
  +remove() bool
  +countQuestion() int
}

class Room {
  +DEFAULT_QUESTION_TIME : const int
  #id_room : int
  #id_quizz : int
  #id_user : int
  #code : string
  #question_time : int
  #auto_kill : DateTime
  #state : EnumRoomState
  #current_question : int
  #time_next_state : DateTime
  #players : Player[]
  #quizz : Quizz
  #nbQuestion : int
  +getId() int
  +setId(int)
  +getIdQuizz() int
  +setIdQuizz(int)
  +getIdOwner() int
  +setIdOwner(int)
  +getCode() string
  +setCode(string)
  +getQuestionTime() int
  +setQuestionTime(int)
  +getAutoKill() DateTime
  +setAutoKill(DateTime)
  +getState() EnumRoomState
  +setState(EnumRoomState)
  +getCurrentQuestion() int
  +setCurrentQuestion(int)
  +getTimeNextState() DateTime
  +setTimeNextState(DateTime)
  +getPlayers() Player[]
  +setPlayers(Player[])
  +getQuizz() Quizz
  +setQuizz(Quizz)
  +getNbQuestion() int
  +setNbQuestion(int)
  +static_generateCode() string
  +addAutoKillMargin()
  +setupTimeNextState()

  %% DB
  +save() bool
  +edit() bool
  +editState() bool
}

class Player {
  <<Not implemented now>>
  -id_player
  -name
  -score
  -current_answer
  +getId()
  +setId(id)
  +getName()
  +setName(name)
  +getScore()
  +setScore(score)
  +getCurrentAnswer()
  +setCurrentAnswer(currentAnswer)
}

class Question {
  #id_question : int
  #id_quizz : int
  #sentence : string
  #priority : int
  #answers: Answer[]
  +getId() int
  +setId(int)
  +getSentence() string
  +setSentence(string)
  +getPriority() int
  +setPriority(int)
  +getAnswers() Answer[]
  +setAnswers(Answer[])
  +getIdQuizz() int
  +setIdQuizz(int)

  %% DB
  +save() bool
  +edit() bool
  +remove() bool
}

class Answer {
  #id_answer : int
  #id_question : int
  #sentence : string
  #is_correct : bool
  #type : EnumAnswer
  +getId() int
  +setId(int)
  +getSentence() string
  +setSentence(string)
  +getIsCorrect() bool
  +setIsCorrect(bool)
  +getType() EnumAnswer
  +setType(EnumAnswer)
  +getIdQuestion() int
  +setIdQuestion(int)

  %% DB
  +save()
  +edit()
  +remove()
}

class EnumAnswer{
  <<enumeration>>
  None
  A
  B
  C
  D
}

class EnumRoomState{
  <<enumeration>>
  Question
  Leaderboard
  Finished 
  Waiting
  None
}

Model <|-- User
Model <|-- Quizz
Model <|-- Room
Model <|-- Player
Model <|-- Question
Model <|-- Answer

User *-- Quizz
User o-- Room
Room *-- Quizz
Room *-- Player
Quizz *-- Question
Question *-- Answer
EnumRoomState <-- Room
EnumAnswer <-- Answer
```