# Description du projet

  

## Titre du projet : Arquizz

  

## Paragraphe explicatif

  Le site permettra d'effectuer des quizz contre d'autres personnes. En soi, il s'agira d'une version "personnelle" du célèbre site "Kahoot".

Une partie (salon) est composée de plusieurs joueurs. Chaque salon comprend plusieurs QCM (question à choix multiples), qui donnent des points aux personnes qui répondent correctement. A la fin de la partie, le joueur ayant le plus haut score gagne.

Un salon est géré par une personne, elle peut choisir les questions, paramétrer diverses options telles que le temps, démarrer / mettre fin au quizz, etc.

L'objectif du projet est de créer un site simple d'utilisation et agréable à l'usage pour participer à des quizz. Les différentes fonctionnalités seront :
* Rejoindre un salon (attente de joueurs)
* Configurer le salon par l'"hôte"
	* Choix des questions
	* Temps maximal pour répondre
	* Nombre de joueurs
* Démarrer la partie
* Création de questions

Les différents QCM existants et les parties en cours sont enregistrés dans une base de données.<br>
Un QCM contient soit 2, soit 4 questions. 1 seul est correcte.

La participation à un QCM ne demande pas au joueur d'être authentifié.
Pour la création des questions, il sera nécessaire d'être authentifié.

Si le temps le permet, les utilisateurs authentifiés pourraient visualiser des statistiques, comme le nombre de questions répondues correctement, combien de quizz ont été faits, etc.

## Objectifs généraux

* Créer un salon :
	* Démarrer le quizz
	* ID pour rejoindre un quizz
* Quizz en cours :
	* Stockage des informations sur l'état du quizz dans la BDD (ID du quizz, liste questions du quizz, question actuelle, etc.)
	* Question suivante automatique
		* Après un temps limite
		* Quand tout le monde a répondu
* Utilisateurs authentifiés :
	* Opérations CRUD sur les quizz
  

## Image de référence

[Kahoot.it](https://kahoot.it/)
![Une question dans Kahoot](https://www.hugochaume.com/wp-content/uploads/2017/05/kahoot-preview.jpg)
