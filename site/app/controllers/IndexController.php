<?php

/**
 * Controller class for the home page.
 */
class IndexController
{
    /**
     * Display the main view of the home (index) page.
     * @return require of the HTML page.
     */
    public function index()
    {
        return Helper::view("index");
    }

    /**
     * Activate the event on the database and redirect to the home page.
     * This is a function to debug the events off from the DB.
     */
    public function activateEventDB()
    {
        Model::activateEventDB();
        Helper::goHome();
    }
}
