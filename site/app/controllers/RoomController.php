<?php

require_once "app/models/Room.php";
require_once "app/models/Quizz.php";
require_once "app/models/Player.php";
require_once "app/models/Answer.php";

/**
 * Controller class for the room page.
 */
class RoomController
{
    /**
     * Minimal alowed time for the states (question/leaderboard)
     */
    const MIN_STATE_TIME = 5;

    /**
     * Display the main view of the room page.
     * @return require of the HTML page.
     */
    public function index()
    {
        $roomCode = $_GET['code'] ?? '';

        $room = Room::fetchCode($roomCode);

        if (!$room) {
            $_SESSION['room_code_invalid'] = "no room with this id";
            Helper::redirect('show-join-room');
        }

        if (!isset($_SESSION['rooms']) || !isset($_SESSION['rooms'][$roomCode])) {
            $_SESSION['player_not_join'] = "player never join this room";
            Helper::redirect('show-join-room');
        } else if (!isset($_SESSION['rooms'][$roomCode]["playerId"])) {
            unset($_SESSION['rooms'][$roomCode]);
            $_SESSION['playerid_join_error'] = "no player id set in the session";
            Helper::redirect('show-join-room');
        }

        $player =  Player::fetchId($_SESSION['rooms'][$roomCode]["playerId"]);

        return Helper::view("room", [
            "room" => $room,
            "player" => $player,
            "information" => [
                "warningMsgAlreadyJoin" => Helper::isSetGetCleanSession('error_already_join_room'),
            ]
        ]);
    }

    public function showJoinRoom()
    {
        return Helper::view("showJoinRoom", [
            "information" => [
                "errorMsgNotExist" => Helper::isSetGetCleanSession('error_join_room_not_exist'), // "Msg" to signify that the element contains the message to display
                "errorMsgAlreadyStarted" => Helper::isSetGetCleanSession('error_join_room_already_started'),
                "errorJoin" => Helper::isSetGetCleanSession('error_join_room')[0],
                "errorPlayerNeverJoin" => Helper::isSetGetCleanSession('player_not_join')[0],
                "errorPlayerId" => Helper::isSetGetCleanSession('playerid_join_error')[0],
                "errorUnknownRoom" => Helper::isSetGetCleanSession('room_code_invalid')[0],
            ]
        ]);
    }

    public function joinRoom()
    {
        $roomCode = $_POST['room_code'] ?? '';
        $playerName = $_POST['player_name'] ?? '<unknown>';

        $room = Room::fetchCode($roomCode);

        // Verify that the room exists
        if (!$room) {
            $_SESSION['error_join_room_not_exist'] = "The room '$roomCode' doesn't exist";
            Helper::redirect('show-join-room');
        }

        if (!isset($_SESSION['rooms'])) {
            $_SESSION['rooms'] = [];
        }
        // Verify that the player didn't already join the specified room
        elseif ($_SESSION['rooms'][$roomCode]) {
            $existingPlayerName = $_SESSION['rooms'][$roomCode]['playerName'];
            if (strcasecmp($existingPlayerName, $playerName)) { // Message only if the name change
                $_SESSION['error_already_join_room'] = "You already join the room '$roomCode' with pseudonyme '$existingPlayerName'";
            }

            // Even if the name is the same, we redirect because we don't want to create a new player
            Helper::redirect("room?code=" . urlencode($roomCode));
        }

        // Verify that the room hasn't started yet
        if ($room->getState() !== EnumRoomState::Waiting) {
            $_SESSION['error_join_room_already_started'] = "The room '$roomCode' already started";
            Helper::redirect('show-join-room');
        }

        $player = new Player();

        $player->setName($playerName);
        $player->setCurrentAnswer(EnumAnswer::None);
        $player->setScore(0);
        $player->setIdRoom($room->getId());

        $dbSuccess = $player->save();

        if (!$dbSuccess) {
            $_SESSION["error_join_room"] = "Failed to join the room";
            Helper::redirect("show-join-room");
        }

        $_SESSION['rooms'][$roomCode] = [
            'playerId' => $player->getId(),
            'playerName' => $playerName,
        ];

        Helper::redirect("room?code=" . urlencode($room->getCode()));
    }

    /**
     * Display the view of the page for create a room.
     * @return require of the HTML page.
     */
    public function showCreateRoom()
    {
        Helper::goHomeIfNotLogged();

        $rooms = Room::fetchAllOfUser($_SESSION["id_user"]);

        $quizzes = Quizz::fetchAll(
            [],
            'id_quizz',
            'ASC'
        );

        //Link quizzes with room to have the names
        foreach ($rooms as $room) {
            $i = 0;
            $found = false;
            do {
                if ($quizzes[$i]->getId() == $room->getIdQuizz()) {
                    $room->setQuizz($quizzes[$i]);
                    $room->setNbQuestion($quizzes[$i]->countQuestion());
                    $found = true;
                }
                $i++;
            } while (!$found && $i < count($quizzes));
            
            if (!$found) // Security if the quizz isn't get from the DB
            {
                $quizz = new Quizz();
                $quizz->setName("already deleted");
                $room->setQuizz($quizz);
                $room->setNbQuestion($room->getCurrentQuestion());
            }
        }


        return Helper::view("showCreateRoom", [
            "rooms" => $rooms,
            "quizzes" => $quizzes,
            "information" => [
                "errorNotBelong" => Helper::issetGetCleanSession('not_belong')[0],
                "errorAlreadyExists" => Helper::issetGetCleanSession('already_exists')[0],
                "errorUnknownAction" => Helper::issetGetCleanSession('unknown_action')[0],
                "errorMissingData" => Helper::issetGetCleanSession('missing_data')[0],
                "errorCreate" => Helper::issetGetCleanSession('error_Create')[0],
                "errorUnknownRoom" => Helper::issetGetCleanSession('unknown_room')[0],
            ],
        ]);
    }

    /**
     * Perform the action to create a room.
     */
    public function createRoom()
    {
        Helper::goHomeIfNotLogged();

        if (!isset($_POST['action_create'])) {
            $_SESSION["unknown_action"] = "unknown action";
            Helper::redirect("show-create-room");
        }

        // - Code verification
        $code = $_POST["roomCode"] ?? "";

        do {
            $tmpCode = $code;
            if ($code == "") //No code -> generate one
            {
                $tmpCode = Room::generateCode();
            }

            // verify if this code is already used by another room, the 
            $roomExists = Room::fetchCode($tmpCode);

            if ($roomExists == false) { // == cause if not false, it's an array
                $code = $tmpCode;
            }
        } while ($code == ""); // the generated code gave an already used code

        if ($roomExists) {
            $_SESSION["already_exists"] = "room with code $code already exists";
            Helper::redirect("show-create-room");
        }


        $room = new Room();
        $room->setIdOwner($_SESSION["id_user"]);
        $room->setCode($code);


        // - Quizz verification
        if (isset($_POST["quizzList"])) {
            $quizzId = $_POST["quizzList"];
            $quizzExist = Quizz::fetchId($quizzId);

            if ($quizzExist != false) { // not !$.. cause if not false, it's an array
                $room->setIdQuizz($_POST["quizzList"]);
            } else {
                $_SESSION["missing_data"] = "No quizz with this id";
                Helper::redirect("show-create-room");
            }
        } else {
            $_SESSION["missing_data"] = "Missing quizz";
            Helper::redirect("show-create-room");
        }

        // - Duration verification
        $duration = Room::DEFAULT_QUESTION_TIME;
        if (isset($_POST["time"]) && $_POST["time"] != "") {
            if (ctype_digit($_POST["time"]) && (int)$_POST["time"] >= self::MIN_STATE_TIME) {
                $duration = (int)$_POST["time"];
            } else {
                $_SESSION["missing_data"] = "Wrong time format";
                Helper::redirect("show-create-room");
            }
        }


        // - finalise room and save it to the DB
        $room->setQuestionTime($duration);
        $room->addAutoKillMargin();
        $room->setState(EnumRoomState::Waiting);

        $dbSuccess = $room->save();

        if (!$dbSuccess) {
            $_SESSION["error_create"] = "Failed to create the room";
            Helper::redirect("show-create-room");
        }

        Helper::redirect("new-room?code=" . urlencode($room->getCode()));
    }

    /**
     * Display the view of the page for new room (to start/stop the room).
     * @return require of the HTML page.
     */
    public function newRoom()
    {
        Helper::goHomeIfNotLogged();

        if (!isset($_GET['code'])) {
            $_SESSION["unknown_room"] = "unknown code";
            Helper::redirect("show-create-room");
        }

        $room = Room::fetchCode($_GET["code"]);

        if ($room == false) {
            $_SESSION["unknown_room"] = "unknown room";
            Helper::redirect("show-create-room");
        }

        // Security if the quiz does not belong to the user
        if ($room->getIdOwner() != $_SESSION["id_user"]) {
            $_SESSION["not_belong"] = "Room not belong to the user";

            Helper::redirect("show-create-room");
        }

        // The view has a JS functionality that retrive the players, no need to handle player here
        // that would by a unnecessary complexity addition

        return Helper::view("newRoom", [
            "room" => $room,
            "information" => [
                "errorUnknownAction" => Helper::issetGetCleanSession('unknown_action')[0],
                "errorCantStart" => Helper::issetGetCleanSession('cant_start')[0],
                "errorAlreadyFinished" => Helper::issetGetCleanSession('already_finished')[0],
                "errorStateUpdate" => Helper::issetGetCleanSession('error_state_update')[0],
            ],
        ]);
    }

    /**
     * Perform the action to start a room.
     */
    public function startRoom()
    {
        list($room, $redirect, $needRedirect) = $this->roomStartStopGuardClose("action_start");

        if ($room->getState() != EnumRoomState::Waiting) {
            $_SESSION["cant_start"] = "room not startable";
            $needRedirect = true;
        }

        if ($needRedirect) {
            Helper::redirect($redirect);
        }

        // All guard clause passed (redirection)
        // Perform the modifications

        // Setup first question conditions
        $room->setState(EnumRoomState::Question);
        $room->setCurrentQuestion(1);
        $room->setupTimeNextState();

        $dbSuccess = $room->editState();
        if (!$dbSuccess) {
            $_SESSION["error_state_update"] = "Failed to update the state of the room";
        }

        Helper::redirect("new-room?code=" . urlencode($room->getCode()));
    }

    /**
     * Perform the action to stop a room.
     */
    public function stopRoom()
    {
        list($room, $redirect, $needRedirect) = $this->roomStartStopGuardClose("action_stop");

        if ($room->getState() == EnumRoomState::Finished) {
            $_SESSION["already_finished"] = "room already finished";
            $needRedirect = true;
        }

        if ($needRedirect) {
            Helper::redirect($redirect);
        }

        // All guard clause passed (redirection)
        // Perform the modifications

        $room->setState(EnumRoomState::Finished);

        $dbSuccess = $room->editState();
        if (!$dbSuccess) {
            $_SESSION["error_state_update"] = "Failed to update the state of the room";
        }

        Helper::redirect("new-room?code=" . urlencode($room->getCode()));
    }

    /**
     * Perform the guard close (user logged, valid code, valid action (start or stop) and if room belong to the user) for an action on the room.
     * @param string $actionName Name of the action to perform get in the $_POST.
     * @return array [1] (Room) Room data get from the DB, [1] (string) redirection path, [2] (bool) is redirection require (to avoid modification on the room).
     */
    private function roomStartStopGuardClose($actionName)
    {
        Helper::goHomeIfNotLogged();

        $code = "";
        $room = new Room();

        $needRedirect = false;
        if (!isset($_POST["code"])) {
            $_SESSION["unknown_room"] = "unknown code";
            $needRedirect = true;
        } else {
            $code = $_POST["code"];

            $room = Room::fetchCode($code);

            if ($room == false) {
                $_SESSION["unknown_room"] = "unknown room";
                $needRedirect = true;
            }
        }

        //Set the redirect page (should be to the room or to the )
        $redirect = "new-room?code=" . urlencode($code);
        if ($needRedirect) {
            $redirect = "show-create-room";
        }

        if (!isset($_POST[$actionName])) {
            $_SESSION["unknown_action"] = "unknown action";
            $needRedirect = true;
        }

        // Security if the room does not belong to the user
        if ($room->getIdOwner() != $_SESSION["id_user"]) {
            $_SESSION["not_belong"] = "Room not belong to the user";
            $redirect = "show-create-room";
            $needRedirect = true;
        }

        return [$room, $redirect, $needRedirect];
    }

    public function continousParsePlayer()
    {
        $json = json_decode(file_get_contents('php://input'), true);

        if (isset($json["idRoom"])) {
            $idRoom = $json["idRoom"];

            $players = Player::fetchAllOfRoom($idRoom);

            $newPlayers = []; // result variable


            if (isset($json["playersId"])) {
                foreach ($players as $player) {

                    $isNewPlayer = true;
                    // Verify if the player is not in the players array
                    foreach ($json["playersId"] as $oldPlayerId) {
                        if ($player->getId() == $oldPlayerId) {
                            $isNewPlayer = false;
                            continue;
                        }
                    }

                    // Store the new player
                    if ($isNewPlayer) {
                        $playerData = [];
                        $playerData["id"] = $player->getId();
                        $playerData["name"] =  $player->getName();

                        array_push($newPlayers, $this->jsSecureParsePlayer($player));
                    }
                }
            } else { // All players are new
                foreach ($players as $player) {
                    array_push($newPlayers, $this->jsSecureParsePlayer($player));
                }
            }

            // Encoding result array in JSON format
            echo json_encode($newPlayers);
        } else {
            echo '{"error": true}';
        }
    }

    /**
     * JS is in client side, so we transmite only limited data for security
     * @param Player $player
     */
    private function jsSecureParsePlayer($player)
    {
        $playerData = [];
        $playerData["id"] = $player->getId();
        $playerData["name"] =  $player->getName();

        return $playerData;
    }

    public function continousParseRoomState()
    {
        $result = [];
        $json = json_decode(file_get_contents('php://input'), true);

        if (isset($json["idRoom"])) {
            $idRoom = $json["idRoom"];

            $room = Room::fetchId($idRoom);
            array_push($result, $this->jsSecureParseRoomState($room));


            // Encoding result array in JSON format
            echo json_encode($result);
        } else {
            echo '{"error": true}';
        }
    }

    /**
     * JS is in client side, so we transmite only limited data for security
     * @param Room $room
     */
    private function jsSecureParseRoomState($room)
    {
        $dtZone = new DateTimeZone('Europe/Zurich'); //Not using CET, because not handle summer time (+1h)
        $dtNow = new DateTime();
        $dtNow->setTimezone($dtZone);

        $result = [];
        //$result["id"] = $room->getId();
        $result["state"] = $room->getState();
        $result["nextStateTime"] =  $room->getTimeNextState();
        $result["dtNow"] = $dtNow;
        $result["remainingTime"] =  $room->getTimeNextState()->getTimestamp() - $dtNow->getTimestamp();;

        return $result;
    }

    public function continousParseLeaderboard()
    {
        $result = [];
        $json = json_decode(file_get_contents('php://input'), true);

        if (isset($json["idRoom"])) {
            $idRoom = $json["idRoom"];

            $room = Room::fetchId($idRoom);

            // Get the correct answer of the current question
            $correctAnswer = $room->getCorrectAnswer();

            // Get the players who have correctly answered the question
            $correctPlayers = [];
            if ($correctAnswer != null) {
                $correctPlayers = Player::fetchAllOfRoomAndAnswer($idRoom, $correctAnswer->getType());
            }

            // Get all players of the room
            $players = Player::fetchAllOfRoom($idRoom);


            array_push($result, $this->jsSecureParseLeaderboard($correctAnswer, $correctPlayers, $players));
            // Encoding result array in JSON format
            echo json_encode($result);
        } else {
            echo '{"error": true}';
        }
    }

    /**
     * JS is in client side, so we transmite only limited data for security
     * @param Answer $answer
     * @param Player $correctPlayers
     * @param Player $players
     */
    private function jsSecureParseLeaderboard($answer, $correctPlayers, $players)
    {
        $result = [];

        // Set answer values
        $result["answer"]["type"] = $answer->getType();
        $result["answer"]["sentence"] = $answer->getSentence();

        // Set basic values for each players of the room
        foreach ($players as $player) {
            $idPlayer = $player->getId();
            $result["players"][$idPlayer]["name"] = $player->getName();
            $result["players"][$idPlayer]["currentScore"] = $player->getScore();
            $result["players"][$idPlayer]["scoreEarned"] = 0;
            $result["players"][$idPlayer]["finalScore"] = $player->getScore();
        }

        // Set the score earned for the correct players
        foreach ($correctPlayers as $player) {
            $scoreEarned =  1; // A function that calcul the score earned ?
            $idPlayer = $player->getId();
            $result["players"][$idPlayer]["scoreEarned"] = $scoreEarned;
            $result["players"][$idPlayer]["finalScore"] += $scoreEarned;
        }

        return $result;
    }
    
    public function continousParseQuestion()
    {
        $result = [];
        $json = json_decode(file_get_contents('php://input'), true);

        $idRoom = $json["idRoom"] ?? '';
        $idPlayer = $json["idPlayer"] ?? '';
        $score = $json["score"] ?? '';

        if ($idRoom === '' || $idPlayer === '' || $score === '')
        {
            echo '{"error": true}';
            exit();
        }

        $room = Room::fetchId($idRoom);

        $player = new Player();
        $player->setId($idPlayer);
        $player->setCurrentAnswer(EnumAnswer::None);
        $player->editAnswer();

        //Score
        $player->setScore($score);
        $player->editScore();
        
        $question = $room->getCurrentQuestionAndAnswers();

        if (!$question)
        {
            echo '{"error": true}';
            exit();
        }

        $result = $this->jsSecureParseQuestion($question);

        echo json_encode($result);
    }

    /**
     * JS is in client side, so we transmite only limited data for security
     * @param Question $question
     */
    function jsSecureParseQuestion($question)
    {
        $result = [];
        
        // Set answer values
        $result["question"]["number"] = $question->getPriority();
        $result["question"]["sentence"] = $question->getSentence();

        // Set basic values for each answers of the question
        foreach ($question->getAnswers() as $answer) {
            $answerType = $answer->getType();
            $result["answers"][$answerType]["sentence"] = $answer->getSentence();
        }

        return $result;
    }

    public function noparseAnswer()
    {
        $json = json_decode(file_get_contents('php://input'), true);

        $idPlayer = $json["idPlayer"] ?? '';
        $answerType = $json["answerType"] ?? '';

        if ($idPlayer === '' || $answerType === '')
        {
            echo '{"error": true}';
            exit();
        }

        $player = new Player();
        $player->setId($idPlayer);
        $player->setCurrentAnswer($answerType);
        $player->editAnswer();
    }

}
