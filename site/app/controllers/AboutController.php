<?php

/**
 * Controller class for the about page.
 */
class AboutController
{
    /**
     * Display the main view of the about page.
     * @return require of the HTML page.
     */
    public function index()
    {
        $company = "HE-Arc";
        return Helper::view("about", ['company' => $company]);
    }
}