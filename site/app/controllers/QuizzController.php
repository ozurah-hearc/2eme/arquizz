<?php

require_once "app/models/Quizz.php";
require_once "app/models/Question.php";
require_once "app/models/Answer.php";
require_once "app/models/User.php";

/**
 * Controller class for the quizz page.
 */
class QuizzController
{
    /**
     * Display the main view of the showQuizzes page.
     * On this page, you can see all quizzes
     * @return require of the HTML page.
     */
    public function index()
    {
        Helper::goHomeIfNotLogged();

        $quizzes = Quizz::fetchAllUndeletedQuizz();

        $users = [];
        foreach ($quizzes as $quizz) {
            $questions = Question::fetchQuestionsOfQuizz($quizz->getId());

            $quizz->setQuestions($questions);

            $users[$quizz->getId()] = User::fetchId($quizz->getIdUser())->getEmail();
        }

        return Helper::view("showQuizzes", [
            "quizzes" => $quizzes,
            "quizzOwners" => $users,
            "information" => [
                "errorNotExists" => Helper::issetGetCleanSession('not_exists')[0],
                "errorNotBelong" => Helper::issetGetCleanSession('not_belong')[0],
                "errorUnknownAction" => Helper::issetGetCleanSession('unknown_action')[0],
                "statusDeleted" => Helper::issetGetCleanSession('deleted'),
                "statusUpdated" => Helper::issetGetCleanSession('updated'),
                "statusCreated" => Helper::issetGetCleanSession('created'),
                "errorFinaliseDb" => Helper::issetGetCleanSession('errorFinaliseDb')[0],
            ],
        ]);
    }

    /**
     * Display view for showing a specific quizz (or create a new one).
     * @return require of the HTML page.
     */
    public function showQuizz()
    {
        Helper::goHomeIfNotLogged();

        if (isset($_GET['id'])) // Edit
        {
            $quizz = Quizz::fetchId($_GET['id']);
            if ($quizz === false) {
                $_SESSION["not_exists"] = "No quizz with this id";

                Helper::redirect("show-quizzes");
            }

            $displayModeRW = true; // if RW true, can edit / delete; else only see the data
            if ($quizz->getIdUser() != $_SESSION["id_user"]) {
                $displayModeRW = false;
            }

            $questions = Question::fetchQuestionsOfQuizz($quizz->getId());

            $quizz->setQuestions($questions);

            for ($i = 0; $i < count($questions); $i++) {
                $currentQuestion = $questions[$i];
                $answers = Answer::fetchAnswersOfQuestion($currentQuestion->getId());

                $currentQuestion->setAnswers($answers);
            }

            Helper::view("showQuizz", [
                "quizz" => $quizz,
                "questions" => $questions,
                "displayRW" => $displayModeRW,
            ]);
        } else { // Create new
            $quizz =  new Quizz();
            $quizz->setName("New quizz");

            Helper::view("showQuizz", [
                "quizz" => $quizz,
                "questions" => [],
            ]);
        }
    }

    /**
     * Perform the action to create, edit or delete a quizz
     */
    public function editQuizz()
    {
        Helper::goHomeIfNotLogged();

        $validOperation = true;
        $dbSuccess = true; // all db querry success ?

        $quizz = new Quizz();
        $quizz->setIdUser($_SESSION["id_user"]);

        $dbh = Model::getDB();
        $dbh->beginTransaction();

        if (isset($_POST['quizzId']) && (isset($_POST['action_delete']) || isset($_POST['action_update']))) {

            // Security if the quiz does not belong to the user
            $quizzBelongVerif = Quizz::fetchId($_POST['quizzId']);
            if ($quizzBelongVerif->getIdUser() != $_SESSION["id_user"]) {
                $_SESSION["not_belong"] = "Quizz not belong to the user";

                Helper::redirect("show-quizzes");
            }

            $quizz->setId($_POST['quizzId']);
        } elseif (isset($_POST['action_create'])) {
            if (!$quizz->save()) {
                $dbSuccess = false;
                $_SESSION["created"] = $dbSuccess;
            }
        } else {
            $validOperation = false;
            $_SESSION["unknown_action"] = "unknown action";
        }


        if ($validOperation && $dbSuccess) {
            if (isset($_POST['action_delete'])) {
                $dbSuccess = $this->onDeleteQuizz($quizz);
                $_SESSION["deleted"] = $dbSuccess;
            } elseif (isset($_POST['action_update'])) {
                $questionsId = $_POST['questions'] ?? [];
                $deletedQuestionsId = $_POST['deleted_questions'] ?? [];

                $dbSuccess =  $this->onEditQuizz($quizz, $questionsId, $deletedQuestionsId);
                $_SESSION["updated"] = $dbSuccess;
            } else { //create
                $questionsId = $_POST['questions'] ?? [];
                // instead of edit question, remplace by creation
                foreach ($questionsId as $key => $prefixedId) {
                    $questionsId[$key] =  substr_replace($prefixedId, "C", 0, 1);
                }

                $dbSuccess =  $this->onEditQuizz($quizz, $questionsId, []);
                $_SESSION["created"] = $dbSuccess;
            }
        }

        try {
            if ($dbSuccess) {
                $dbh->commit();
            }
        } catch (PDOException $e) {
            $dbSuccess = false;
            $_SESSION["errorFinaliseDb"] = true;
            if (isset($_POST['action_delete'])) $_SESSION["deleted"] = false;
            if (isset($_POST['action_update'])) $_SESSION["updated"] = false;
            if (isset($_POST['action_create'])) $_SESSION["created"] = false;
        }


        if (!$dbSuccess) {
            $dbh->rollBack();
        }

        Helper::redirect("show-quizzes");
    }

    /**
     * Manage + apply modification (and save them to the DB) for quizz, question and answer, when edition or creation of a quizz
     * @param Quizz $quizz Linked quizz of the question and answers
     * @param int[] $questionsId ID of the questions ordred by priority
     * @param int[] $deletedQuestionsId questions to delete
     * @return bool is the edition/creation work correctly ?
     */
    private function onEditQuizz(Quizz $quizz, $questionsId, $deletedQuestionsId)
    {
        $quizzId = $quizz->getId();
        $quizz->setName($_POST["quizzName"] ?? "");

        if (!$quizz->edit()) return false;

        $priority = 0;

        //update+create questions
        foreach ($questionsId as $prefixedId) {
            $priority++;

            preg_match('/^[CE]_[0-9]+$/', $prefixedId, $matches);

            if (count($matches) === 0) continue;

            $prefixLetter = $prefixedId[0]; // If a question is edited, it will be "E", else if created, it will be "C"

            $questionId = substr($prefixedId, 2);

            if ($prefixLetter === 'E') {


                $question = Question::fetchId($questionId);

                $answers = Answer::fetchAnswersOfQuestion($questionId);

                $questionSentence = $_POST["question{$questionId}"] ?? '<unknown>';
                $answersSentence  = $_POST["answers{$questionId}"] ?? [];
                $correctAnswerType  = $_POST["correct{$questionId}"] ?? EnumAnswer::None;


                if (count($answersSentence) !== 4) continue;

                $question->setSentence($questionSentence);
                $question->setPriority($priority);


                $answers[0]->setSentence($answersSentence[0]);
                $answers[1]->setSentence($answersSentence[1]);
                $answers[2]->setSentence($answersSentence[2]);
                $answers[3]->setSentence($answersSentence[3]);

                $answers[0]->setIsCorrect($answers[0]->getType() === $correctAnswerType);
                $answers[1]->setIsCorrect($answers[1]->getType() === $correctAnswerType);
                $answers[2]->setIsCorrect($answers[2]->getType() === $correctAnswerType);
                $answers[3]->setIsCorrect($answers[3]->getType() === $correctAnswerType);

                $question->setAnswers($answers);

                if (!$answers[0]->edit()) return false;
                if (!$answers[1]->edit()) return false;
                if (!$answers[2]->edit()) return false;
                if (!$answers[3]->edit()) return false;

                if (!$question->edit()) return false;
            } else if ($prefixLetter === 'C') {
                $questionSentence  = $_POST["question{$questionId}"] ?? '<unknown>';
                $answersSentence   = $_POST["answers{$questionId}"] ?? [];
                $correctAnswerType = $_POST["correct{$questionId}"] ?? EnumAnswer::None;
                
                if (count($answersSentence) !== 4) continue;

                $question = new Question();

                $question->setIdQuizz($quizzId);
                $question->setSentence($questionSentence);
                $question->setPriority($priority);

                if (!$question->save()) return false;

                $questionId = $question->getId();

                $answers = [
                    new Answer(),
                    new Answer(),
                    new Answer(),
                    new Answer()
                ];

                $answers[0]->setIdQuestion($questionId);
                $answers[1]->setIdQuestion($questionId);
                $answers[2]->setIdQuestion($questionId);
                $answers[3]->setIdQuestion($questionId);

                $answers[0]->setSentence($answersSentence[0]);
                $answers[1]->setSentence($answersSentence[1]);
                $answers[2]->setSentence($answersSentence[2]);
                $answers[3]->setSentence($answersSentence[3]);

                $answers[0]->setType(EnumAnswer::A);
                $answers[1]->setType(EnumAnswer::B);
                $answers[2]->setType(EnumAnswer::C);
                $answers[3]->setType(EnumAnswer::D);

                $answers[0]->setIsCorrect($correctAnswerType === EnumAnswer::A);
                $answers[1]->setIsCorrect($correctAnswerType === EnumAnswer::B);
                $answers[2]->setIsCorrect($correctAnswerType === EnumAnswer::C);
                $answers[3]->setIsCorrect($correctAnswerType === EnumAnswer::D);

                if (!$answers[0]->save()) return false;
                if (!$answers[1]->save()) return false;
                if (!$answers[2]->save()) return false;
                if (!$answers[3]->save()) return false;
            }
        }

        //delete questions
        foreach ($deletedQuestionsId as $questionId) {
            $question = new Question();
            $question->setId($questionId);

            // No need to remove answere first, because the DB handle it (drop cascade)

            if (!$question->remove()) return false;
        }

        return true;
    }

    /**
     * Manage + remove the quizz from the DB
     * @param Quizz $quizz quizz to delete
     * @return bool is the deletion work correctly ?
     */
    private function onDeleteQuizz(Quizz $quizz)
    {
        return $quizz->remove();
    }
}
