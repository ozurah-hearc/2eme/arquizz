<?php

require_once "app/models/User.php";

/**
 * Controller class for the login (connection and deconnection) page.
 */
class ConnectionController
{
	/**
	 * Display the main view of the connection page.
	 * @return require of the HTML page.
	 */
	public function index()
	{
		$isInvalid = Helper::issetGetCleanSession('invalid')[0];

		if (isset($_SESSION['id_user'])) {
			Helper::goHome();
		}

		return Helper::view("connection", [
			"information" => ['errorInvalid' => $isInvalid],
		]);
	}

	/**
	 * Perform the connection of the user according his credentials and the data on the DB.
	 * * If login success : redirect to the home page
	 * * If login failed : redirect to the connexion page
	 */
	public function connect()
	{
		if (
			$_SERVER['REQUEST_METHOD'] !== 'POST'
			|| !isset($_POST['email'])
			|| !isset($_POST['password'])
		) {
			helper::goHome();
		}

		$user = new User();
		$user->setEmail($_POST['email']);

		$authSuccess = $user->authenticate($_POST['password']);
		$success = $authSuccess ? "success" : "fail";
		Log::logMessage('login', "email={$_POST['email']}, status=$success");

		if (!$authSuccess) {
			$_SESSION["invalid"] = true;
			Helper::redirect("connection");
		}

		$this->keepConnectedUser($user);

		helper::goHome();
	}

	/**
	 * Perform the deconnection of the user.
	 * Redirect to the home page
	 */
	public function disconnect()
	{
		Log::logMessage('logout', "email={$_SESSION['email']}, success");
		unset($_SESSION['id_user']);
		unset($_SESSION['email']);
		helper::goHome();
	}

	/**
	 * Display the view of the create account page.
	 * @return require of the HTML page.
	 */
	public function showCreateAccount()
	{
		if (isset($_SESSION['id_user'])) {
			Helper::goHome();
		}


		$errorIsInvalid = Helper::issetGetCleanSession('createInvalid')[0];
		$errorIsAlreadyExists = Helper::issetGetCleanSession('createAlreadyExists')[0];
		$errorIsPasswordNotMatch = Helper::issetGetCleanSession('passwordNotMatch')[0];

		return Helper::view("createAccount", [
			"information" => [
				'errorInvalid' => $errorIsInvalid,
				'errorAlreadyExists' => $errorIsAlreadyExists,
				'errorPasswordNotMatch' => $errorIsPasswordNotMatch
			],
		]);
	}

	/**
	 * Perform the action for create an account
	 */
	public function createAccount()
	{
		if (isset($_SESSION['id_user'])) {
			Helper::goHome();
		}

		if (
			$_SERVER['REQUEST_METHOD'] !== 'POST'
			|| !isset($_POST['email'])
			|| !isset($_POST['password'])
			|| !isset($_POST['passwordVerification'])
		) {
			$_SESSION["createInvalid"] = true;
			Helper::redirect("show-create-account");
		}

		if ($_POST['password'] !== $_POST['passwordVerification']) {
			$_SESSION["passwordNotMatch"] = true;
			Helper::redirect("show-create-account");
		}

		$user = new User();
		$user->setEmail($_POST['email']);
		$user->setAndHashPassword($_POST['password']);

		$success = $user->createAccount();

		if (!$success) {
			$_SESSION["createAlreadyExists"] = true;
			Helper::redirect("show-create-account");
		} else {
			//Create the account done, auto connect the user
			$this->keepConnectedUser($user);

			helper::goHome();
		}
	}

	/**
	 * Store the email and user id to the session
	 */
	private function keepConnectedUser($user)
	{
		$_SESSION['id_user'] = $user->getId();
		$_SESSION['email'] = $user->getEmail();
	}
}
