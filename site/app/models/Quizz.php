<?php
require_once "app/models/Question.php";

/**
 * The quizz class (information about a quizz).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class Quizz extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'Quizz'; //used in Model
    protected static $PK_ID_NAME = "id_quizz"; //used in Model
    protected static $TABLE_NAME = 'Quizz'; //used in Model

    protected $questions; // Question[] object aggregation
    protected $rooms; // Room[] object aggregation

    // --------------- DB Attributes --------------- //

    protected $id_quizz = -1;
    protected $name = "";
    protected $deletion = false;
    protected $id_user = -1;

    // ----------- Getters and Setters (DB sources) ---------- //

    /**
     * Get the id of the quizz.
     * @return int ID of the quizz.
     */
    public function getId()
    {
        return $this->id_quizz;
    }

    /**
     * Set the id of the quizz.
     * @param int $value new id of the quizz.
     */
    public function setId($value)
    {
        $this->id_quizz = $value;
    }

    /**
     * Get the name of the quizz.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the name of the quizz.
     * @param string $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /**
     * Get the deletion state of the quizz.
     * (If true, the DB can delete it)
     * @return bool
     */
    public function getDeletion()
    {
        return $this->deletion;
    }

    /**
     * Set the deletion state of the quizz.
     * (If true, the DB can delete it)
     * @param bool $value
     */
    public function setDeletion($value)
    {
        $this->deletion = $value;
    }

    /**
     * Get the id of creator of this quizz.
     * @return int
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * Set the id of creator of this quizz.
     * @param int $value
     */
    public function setIdUser($value)
    {
        $this->id_user = $value;
    }

    // ----------- Getters and Setters (not DB sources) ---------- //

    /**
     * Get the rooms that use this quizz.
     * (this value isn't get from the DB, it should be setted before)
     * @return Room[]
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set the rooms that use this quizz.
     * @param Room[] $value
     */
    public function setRooms($value)
    {
        $this->rooms = $value;
    }

    /**
     * Get the questions of this quizz.
     * (this value isn't get from the DB, it should be setted before)
     * @return Question[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set the questions of this quizz.
     * @param Question[] $value new id of the quizz.
     */
    public function setQuestions($value)
    {
        $this->questions = $value;
    }

    // --------------- Constructor -------------- //

    public function __construct()
    {
    }

    // --------------- Public DB Method ------------ //

    /**
     * Save this instance to the DB.
     */
    public function save()
    {
        $values = [
            ['name', $this->getName(), PDO::PARAM_STR],
            ['deletion', $this->getDeletion(), PDO::PARAM_BOOL],
            ['id_user', $this->getIdUser(), PDO::PARAM_INT],
        ];

        list($success, $lastInsertId) = parent::create($values, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }

    /**
     * Edit this instance to the DB.
     * @return bool did the update work correctly ?
     */
    public function edit()
    {
        $values = [
            ['name', $this->getName(), PDO::PARAM_STR],
            ['deletion', $this->getDeletion(), PDO::PARAM_BOOL],
            ['id_user', $this->getIdUser(), PDO::PARAM_INT],
        ];

        $filters = [
            ['id_quizz', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Mention to remove this instance to the DB
     * (the remove is isn't instant, this is the DB who delete at periodic moment)
     * @return bool did mention of the deletion work correctly ?
     */
    public function remove()
    {
        $this->setDeletion(true);

        $values = [
            ['deletion', $this->getDeletion(), PDO::PARAM_BOOL],
        ];

        $filters = [
            ['id_quizz', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Get the quantity of question of this quizz, from the DB
     * @return int number of question of this quizz (-1 if error with the DB)
     */
    public function countQuestion()
    {
        $dbh = self::getDB();
        $tableName = Question::getTableName();

        $req = "SELECT COUNT(*) FROM $tableName WHERE id_quizz = :id";

        $statement = $dbh->prepare($req);


        $nb = -1;
        try {
            $statement->execute(["id" => $this->getId()]);
            $nb = $statement->fetchColumn();
        } catch (PDOException $Exception) {
            // Nothing to do
            $forBreakpoints = $Exception;
        }

        return $nb;
    }

    /**
     * Get all undeleted quizzes from the DB
     * @return Quizz[]
     */
    public static function fetchAllUndeletedQuizz()
    {
        return self::fetchAll(
            [
                ['deletion', false, PDO::PARAM_INT],
            ],
            'id_quizz',
            'ASC'
        );
    }
}
