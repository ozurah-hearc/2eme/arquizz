<?php

/**
 * The question class (information about a question).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class Question extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'Question'; //used in Model
    protected static $PK_ID_NAME = "id_question"; //used in Model
    protected static $TABLE_NAME = 'Question'; //used in Model

    protected $answers = []; // Answer object aggregation

    // --------------- DB Attributes --------------- //

    protected $id_question = -1;
    protected $id_quizz = -1;

    protected $sentence = "";
    protected $priority = 0;

    // ----------- Getters and Setters (DB sources) ---------- //

    /**
     * Get the id of the question.
     * @return int ID of the question.
     */
    public function getId()
    {
        return $this->id_question;
    }

    /**
     * Set the id of the question.
     * @param int $value new id of the question.
     */
    public function setId($value)
    {
        $this->id_question = $value;
    }

    /**
     * Get the sentence
     * @return string
     */
    public function getSentence()
    {
        return $this->sentence;
    }

    /**
     * Set the sentence
     * @param string $value
     */
    public function setSentence($value)
    {
        $this->sentence = $value;
    }

    /**
     * Get the priority
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set the priority
     * @param int $value
     */
    public function setPriority($value)
    {
        $this->priority = $value;
    }

    /**
     * Get the value of id_quizz
     * @return int
     */
    public function getIdQuizz()
    {
        return $this->id_quizz;
    }

    /**
     * Set the value of id_quizz
     * @param int $value
     */
    public function setIdQuizz($value)
    {
        $this->id_quizz = $value;
    }

    // ----------- Getters and Setters (not DB sources) ---------- //

    /**
     * Get the answers
     * (this value isn't get from the DB, it should be setted before)
     * @return Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set the answers
     * @param Answer[] $value
     */
    public function setAnswers($value)
    {
        $this->answers = $value;
    }

    // --------------- Constructor -------------- //

    public function __construct()
    {
    }

    // --------------- Public DB Method ------------ //

    /**
     * Save this instance to the DB.
     */
    public function save()
    {
        $values = [
            ['sentence', $this->getSentence(), PDO::PARAM_STR],
            ['priority', $this->getPriority(), PDO::PARAM_INT],
            ['id_quizz', $this->getIdQuizz(), PDO::PARAM_INT],
        ];

        list($success, $lastInsertId) = parent::create($values, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }

    /**
     * Edit this instance to the DB.
     * @return bool did the update work correctly ?
     */
    public function edit()
    {
        $values = [
            ['sentence', $this->getSentence(), PDO::PARAM_STR],
            ['priority', $this->getPriority(), PDO::PARAM_INT],
            ['id_quizz', $this->getIdQuizz(), PDO::PARAM_INT],
        ];

        $filters = [
            ['id_question', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Remove this instance to the DB
     * @return bool did the deletion work correctly ?
     */
    public function remove()
    {
        $values = [
            ['id_question', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::delete($values, true);
    }

    /**
     * Get the questions of the specified quizz from the DB.
     * @param int $quizzId ID of the quizz.
     * @return Question[]
     */
    public static function fetchQuestionsOfQuizz($quizzId)
    {
        return self::fetchAll(
            [
                ['id_quizz', $quizzId, PDO::PARAM_INT],
            ],
            'priority',
            'ASC'
        );
    }
}
