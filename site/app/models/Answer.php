<?php

/**
 * The answer class (information about a answer).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class Answer extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'Answer'; //used in Model
    protected static $PK_ID_NAME = "id_answer"; //used in Model
    protected static $TABLE_NAME = 'Answer'; //used in Model

    // --------------- DB Attributes --------------- //

    protected $id_answer = -1;
    protected $id_question = -1;

    protected $sentence = "";
    protected $is_correct = false;
    protected $type = EnumAnswer::None;

    // ----------- Getters and Setters ---------- //

    /**
     * Get the id of the answer.
     * @return int ID of the answer.
     */
    public function getId()
    {
        return $this->id_answer;
    }

    /**
     * Set the id of the answer.
     * @param int $value new id of the answer.
     */
    public function setId($value)
    {
        $this->id_answer = $value;
    }

    /**
     * Get the sentence
     * @return string
     */
    public function getSentence()
    {
        return $this->sentence;
    }

    /**
     * Set the sentence
     * @param string $value
     */
    public function setSentence($value)
    {
        $this->sentence = $value;
    }

    /**
     * Get the value of is_correct
     * @return bool
     */
    public function getIsCorrect()
    {
        return $this->is_correct;
    }

    /**
     * Set the value of is_correct
     * @param bool $value
     */
    public function setIsCorrect($value)
    {
        $this->is_correct = $value;
    }

    /**
     * Get the type (order of the answer)
     * @return EnumAnswer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type (order of the answer)
     * @param EnumAnswer $value
     */
    public function setType($value)
    {
        $this->type = $value;
    }

    /**
     * Get the id of the asociated question
     * @return int
     */
    public function getIdQuestion()
    {
        return $this->id_question;
    }

    /**
     * Set the id of the asociated question
     * @param int $value
     */
    public function setIdQuestion($value)
    {
        $this->id_question = $value;

        return $this;
    }

    // --------------- Constructor -------------- //

    public function __construct()
    {
    }

    // --------------- Public DB Method ------------ //

    /**
     * Save this instance to the DB.
     */
    public function save()
    {
        $values = [
            ['sentence', $this->getSentence(), PDO::PARAM_STR],
            ['is_correct', $this->getIsCorrect(), PDO::PARAM_BOOL],
            ['type', $this->getType(), PDO::PARAM_STR],
            ['id_question', $this->getIdQuestion(), PDO::PARAM_INT],
        ];

        list($success, $lastInsertId) = parent::create($values, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }

    /**
     * Edit this instance to the DB.
     * @return bool did the update work correctly ?
     */
    public function edit()
    {
        $values = [
            ['sentence', $this->getSentence(), PDO::PARAM_STR],
            ['is_correct', $this->getIsCorrect(), PDO::PARAM_BOOL],
            ['type', $this->getType(), PDO::PARAM_STR],
            ['id_question', $this->getIdQuestion(), PDO::PARAM_INT],
        ];

        $filters = [
            ['id_answer', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Remove this instance to the DB
     * @return bool did the deletion work correctly ?
     */
    public function remove()
    {
        $values = [
            ['id_answer', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::delete($values, true);
    }

    /**
     * Get the answers of the specified question from the DB.
     * @param int $questionId ID of the quizz.
     * @return Answer[]
     */
    public static function fetchAnswersOfQuestion($questionId)
    {
        return self::fetchAll(
            [
                ['id_question', $questionId, PDO::PARAM_INT],
            ],
            'type',
            'ASC'
        );
    }
}
