<?php
 require_once "app/models/Question.php";
 require_once "app/models/Answer.php";

/**
 * The room class (information about a room).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class Room extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'Room'; //used in Model
    protected static $PK_ID_NAME = "id_room"; //used in Model
    protected static $TABLE_NAME = 'Room'; //used in Model

    const DEFAULT_QUESTION_TIME = 30;

    protected $players = []; // Player[] object aggregation
    protected $quizz; // Quizz object aggregation
    protected $nbQuestion = 0;

    // --------------- DB Attributes --------------- //

    protected $id_room = -1;
    protected $id_quizz = -1;
    protected $id_user = -1;

    protected $code = "";
    protected $question_time = 0;
    protected $auto_kill; //default in constructor
    protected $state = EnumRoomState::None;
    protected $current_question = 0;
    protected $time_next_state; //default in constructor

    // ----------- Getters and Setters (DB sources) ---------- //

    /**
     * Get the id of the room.
     * @return int ID of the room.
     */
    public function getId()
    {
        return $this->id_room;
    }

    /**
     * Set the id of the room.
     * @param int $value new id of the room.
     */
    public function setId($value)
    {
        $this->id_room = $value;
    }

    /**
     * Get the id of the quizz used in this room.
     * @return int
     */
    public function getIdQuizz()
    {
        return $this->id_quizz;
    }

    /**
     * Set the id of the quizz used in this room.
     * @param int $value
     */
    public function setIdQuizz($value)
    {
        $this->id_quizz = $value;
    }

    /**
     * Get the id user who owned this room.
     * @return int 
     */
    public function getIdOwner()
    {
        return $this->id_user;
    }

    /**
     * Set the id of user who owned this room.
     * @param int $value
     */
    public function setIdOwner($value)
    {
        $this->id_user = $value;
    }

    /**
     * Get the value of the code to join the room
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of the code to join the room
     * @param string $value
     */
    public function setCode($value)
    {
        $this->code = $value;
    }

    /**
     * Get the value of duration (in seconds) of a question
     * @return int
     */
    public function getQuestionTime()
    {
        return $this->question_time;
    }

    /**
     * Set the value of duration (in seconds) of a question
     * @param int $value
     */
    public function setQuestionTime($value)
    {
        $this->question_time = $value;
    }

    /**
     * Get the value of the date of auto delete of the room
     * @return DateTime
     */
    public function getAutoKill()
    {
        return $this->auto_kill;
    }

    /**
     * Set the value of the date of auto delete of the room
     * @param DateTime $value
     */
    public function setAutoKill($value)
    {
        $this->auto_kill = $value;
    }

    /**
     * Get the value of the current state of the room
     * @return EnumRoomState
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of the current state of the room
     * @param EnumRoomState $value
     */
    public function setState($value)
    {
        $this->state = $value;
    }

    /**
     * Get the value of the current question
     * @return int
     */
    public function getCurrentQuestion()
    {
        return $this->current_question;
    }

    /**
     * Set the value of the current question
     * @param int $value
     */
    public function setCurrentQuestion($value)
    {
        $this->current_question = $value;
    }

    /**
     * Get the value of time of the next room state
     * @return DateTime
     */
    public function getTimeNextState()
    {
        return $this->time_next_state;
    }

    /**
     * Set the value of time of the next room state
     * @param DateTime $value
     */
    public function setTimeNextState($value)
    {
        $this->time_next_state = $value;
    }

    // ----------- Getters and Setters (not DB sources) ---------- //

    /**
     * Get the value of players in the room
     * (this value isn't get from the DB, it should be setted before)
     * @return Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Set the value of players in the room
     * @param Player[] $value
     */
    public function setPlayers($value)
    {
        $this->players = $value;
    }

    /**
     * Get the value of the linked quizz of the room
     * (this value isn't get from the DB, it should be setted before)
     * @return Quizz
     */
    public function getQuizz()
    {
        return $this->quizz;
    }

    /**
     * Set the value of the linked quizz of the room
     * @param Quizz $value
     */
    public function setQuizz($value)
    {
        $this->quizz = $value;
    }

    /**
     * Get the value of the number of questions
     * (this value isn't get from the DB, it should be setted before)
     * @return int
     */
    public function getNbQuestion()
    {
        return $this->nbQuestion;
    }

    /**
     * Set the value of the number of questions
     * @param int $value
     */
    public function setNbQuestion($value)
    {
        $this->nbQuestion = $value;
    }

    // --------------- Constructor -------------- //

    public function __construct()
    {
        $dtZone = new DateTimeZone('Europe/Zurich'); //Not using CET, because not handle summer time (+1h)
        $dtNow = new DateTime();
        $dtNow->setTimezone($dtZone);

        //check if string and not empty because pdo fetch_class happens before the constructor
        if (is_string($this->getAutoKill()) && $this->getAutoKill() != "") {
            $this->setAutoKill(new DateTime((string)$this->getAutoKill(), $dtZone));
        } else {
            $this->setAutoKill($dtNow);
        }

        if (is_string($this->getTimeNextState()) && $this->getTimeNextState() != "") {
            $this->setTimeNextState(new DateTime((string)$this->getTimeNextState(), $dtZone));
        } else {
            $this->setTimeNextState($dtNow);
        }
    }

    // --------------- Public Method ------------ //

    /**
     * Generate a unique code
     */
    public static function generateCode()
    {
        return uniqid(); // Always generate a code with 13 characters
    }

    /**
     * Add a margin of 1 day before the auto delete of this room to "auto kill"
     */
    public function addAutoKillMargin()
    {
        $this->getAutoKill()->add(new DateInterval('P1D')); // add 1d
    }

    /**
     * add "question time" to the current DateTime and store it to "time next state"
     */
    public function setupTimeNextState()
    {
        $add = $this->getQuestionTime();

        $dtZone = new DateTimeZone('Europe/Zurich'); //Not using CET, because not handle summer time (+1h)
        $dtNow = new DateTime();
        $dtNow->setTimezone($dtZone);
        $dtNow->add(new DateInterval("PT{$add}S"));

        $this->setTimeNextState($dtNow);
    }

    // --------------- Public DB Method ------------ //

    /**
     * Save this instance to the DB.
     */
    public function save()
    {
        $values = [
            ['code', $this->getCode(), PDO::PARAM_STR],
            ['question_time', $this->getQuestionTime(), PDO::PARAM_INT],
            ['auto_kill', $this->getAutoKill()->format('Y-m-d H-i-s'), PDO::PARAM_STR],
            ['state', $this->getState(), PDO::PARAM_STR],
            ['current_question', $this->getCurrentQuestion(), PDO::PARAM_INT],
            ['time_next_state', $this->getTimeNextState()->format('Y-m-d H-i-s'), PDO::PARAM_STR],
            ['id_quizz', $this->getIdQuizz(), PDO::PARAM_INT],
            ['id_user', $this->getIdOwner(), PDO::PARAM_INT],
        ];

        list($success, $lastInsertId) = parent::create($values, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }

    /**
     * Edit this instance to the DB.
     * @return bool did the update work correctly ?
     */
    public function edit()
    {
        $values = [
            ['question_time', $this->getQuestionTime(), PDO::PARAM_INT],
            ['auto_kill', $this->getAutoKill()->format('Y-m-d H-i-s'), PDO::PARAM_STR],
            ['state', $this->getState(), PDO::PARAM_STR],
            ['current_question', $this->getCurrentQuestion(), PDO::PARAM_INT],
            ['time_next_state', $this->getTimeNextState()->format('Y-m-d H-i-s'), PDO::PARAM_STR],
            ['id_quizz', $this->getIdQuizz(), PDO::PARAM_INT],
            ['id_user', $this->getIdOwner(), PDO::PARAM_INT],
        ];

        $filters = [
            ['id_room', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Edit only the state part of this instance to the DB.
     * (state part means : state; current_question; time_next_state)
     * @return bool did the update work correctly ?
     */
    public function editState()
    {
        $values = [
            ['state', $this->getState(), PDO::PARAM_STR],
            ['current_question', $this->getCurrentQuestion(), PDO::PARAM_INT],
            ['time_next_state', $this->getTimeNextState()->format('Y-m-d H-i-s'), PDO::PARAM_STR],
        ];

        $filters = [
            ['id_room', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    public function getCorrectAnswer()
    {
        $dbh = self::getDB();
        $tableNameRoom = Room::getTableName();
        $tableNameQuestion = Question::getTableName();
        $tableNameAnswer = Answer::getTableName();

        $req = "SELECT * FROM $tableNameAnswer 
                WHERE id_answer = (
                    SELECT id_answer FROM $tableNameAnswer
                                JOIN $tableNameQuestion
                                NATURAL JOIN $tableNameRoom
                                WHERE $tableNameAnswer.id_question = $tableNameQuestion.id_question AND id_room = :id AND is_correct = True
                                ORDER BY priority
                                LIMIT :currentQuestion,1
                )";

        $statement = $dbh->prepare($req);
        $statement->setFetchMode(PDO::FETCH_CLASS, Answer::getClassName());

        $correctAnswer = null;
        try {
            //Why not set param in execute : limit must be typed as INT : https://stackoverflow.com/questions/21673954/pdo-executeparams-vs-bindparam
            //Why variables : https://stackoverflow.com/questions/33768806/php-pdo-only-variables-should-be-passed-by-reference
            //We create new var instead using directly the instance variable to keep the same logic (using the getter) with other classes
            $id = $this->getId();
            $currentQuestion = $this->getCurrentQuestion() - 1;

            $statement->bindParam('id', $id, PDO::PARAM_INT);
            $statement->bindParam('currentQuestion', $currentQuestion, PDO::PARAM_INT);
            $statement->execute();
            $correctAnswer =  $statement->fetch();
        } catch (PDOException $Exception) {
            // Nothing to do
            // Can happen if currentQuestion is 0 (limit will be -1, and provoc an exception)
            $forBreakpoints = $Exception;
        }

        if (!$correctAnswer) { // Is false if answer was not found
            return null;
        }
        return $correctAnswer;
    }

    public function getCurrentQuestionAndAnswers() // TODO : Return question with "getAnswers" setted
    {
        // Retrive the question
        $dbh = self::getDB();
        $tableNameRoom = Room::getTableName();
        $tableNameQuestion = Question::getTableName();

        $req = "SELECT * FROM $tableNameQuestion 
                WHERE id_question = (
                    SELECT id_question FROM $tableNameQuestion
                                NATURAL JOIN $tableNameRoom
                                WHERE id_room = :id
                                ORDER BY priority
                                LIMIT :currentQuestion,1
                )";

        $statement = $dbh->prepare($req);
        $statement->setFetchMode(PDO::FETCH_CLASS, Question::getClassName());

        $question = null;
        try {
            //Why not set param in execute : limit must be typed as INT : https://stackoverflow.com/questions/21673954/pdo-executeparams-vs-bindparam
            //Why variables : https://stackoverflow.com/questions/33768806/php-pdo-only-variables-should-be-passed-by-reference
            //We create new var instead using directly the instance variable to keep the same logic (using the getter) with other classes
            $id = $this->getId();
            $currentQuestion = $this->getCurrentQuestion() - 1;

            $statement->bindParam('id', $id, PDO::PARAM_INT);
            $statement->bindParam('currentQuestion', $currentQuestion, PDO::PARAM_INT);
            $statement->execute();
            $question =  $statement->fetch();
        } catch (PDOException $Exception) {
            // Can happen if currentQuestion is 0 (limit will be -1, and provoc an exception)
            $forBreakpoints = $Exception;

            return null;
        }

        $questionId = $question->getId();

        // Retrive the answers
        $answers = Answer::fetchAll([
            ['id_question', $questionId, PDO::PARAM_INT]
        ], 'type', 'ASC');


        if (count($answers) !== 4) return null;
        $question->setAnswers($answers);

        return $question;
    }

    /**
     * Get the room which have the specified code
     * @param string $code code of the room
     * @return Room
     */
    public static function fetchCode($code)
    {
        return self::fetch(
            [
                ['code', $code, PDO::PARAM_STR],
            ]
        );
    }

    /**
     * Fetch all rooms of the specified user
     * @param int $idUser id of the user
     * @return Rooms[] rooms ordred by name
     */
    public static function fetchAllOfUser($idUser)
    {
        return self::fetchAll(
            [
                ['id_user', $idUser, PDO::PARAM_INT],
            ],
            'id_room',
            'ASC'
        );
    }
}
