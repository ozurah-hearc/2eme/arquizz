<?php
// TODO : ADD variable according the DB
// + Getter/Setter + Methods

/**
 * The player class (information about a player).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class Player extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'Player'; //used in Model
    protected static $PK_ID_NAME = "id_player"; //used in Model
    protected static $TABLE_NAME = 'Player'; //used in Model

    // --------------- DB Attributes --------------- //

    protected $id_player;
    protected $name = "";
    protected $current_answer = "";
    protected $score = "";
    protected $id_room;

    // ----------- Getters and Setters ---------- //

    /**
     * Get the id of the player.
     * @return int ID of the player.
     */
    public function getId()
    {
        return $this->id_player;
    }

    /**
     * Set the id of the player.
     * @param int $value new id of the player.
     */
    public function setId($value)
    {
        $this->id_player = $value;
    }

    /**
     * Get the name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the name
     * @param string $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /**
     * Get the current answer
     * @return EnumAnswer
     */
    public function getCurrentAnswer()
    {
        return $this->current_answer;
    }

    /**
     * Set the current answer
     * @param EnumAnswer $value
     */
    public function setCurrentAnswer($value)
    {
        $this->current_answer = $value;
    }

    /**
     * Get the score
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set the score
     * @param int $value
     */
    public function setScore($value)
    {
        $this->score = $value;
    }

    /**
     * Get the id of the room.
     * @return int ID of the room.
     */
    public function getIdRoom()
    {
        return $this->id_room;
    }

    /**
     * Set the id of the room.
     * @param int $value new id of the room.
     */
    public function setIdRoom($value)
    {
        $this->id_room = $value;
    }



    // --------------- Constructor -------------- //

    public function __construct()
    {
    }

    // --------------- Public DB Method ------------ //
    
    /**
     * Save this instance to the DB.
     */
    public function save()
    {
        $values = [
            ['name', $this->getName(), PDO::PARAM_STR],
            ['score', $this->getScore(), PDO::PARAM_INT],
            ['current_answer', $this->getCurrentAnswer(), PDO::PARAM_STR],
            ['id_room', $this->getIdRoom(), PDO::PARAM_INT],
        ];

        list($success, $lastInsertId) = parent::create($values, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }

    /**
     * Update the answer of this instance to the DB
     */
    public function editAnswer()
    {

        $values = [
            ['current_answer', $this->getCurrentAnswer(), PDO::PARAM_STR],
        ];

        $filters = [
            ['id_player', $this->getId(), PDO::PARAM_INT],
        ];

        return parent::update($values, $filters, true);
    }

    /**
     * Update the score of this instance to the DB.
     */
    public function editScore()
    {
        $values = [
            ['score', $this->getScore(), PDO::PARAM_INT],
        ];

        $filters = [
            ['id_player', $this->getId(), PDO::PARAM_INT],
        ];
        
        return parent::update($values, $filters, true);
    }

    /**
     * Fetch all players of the specified room
     * @param int $idRoom id of the room
     * @return Player[] players ordred by name
     */
    public static function fetchAllOfRoom($idRoom)
    {
        return self::fetchAll([
            ['id_room', $idRoom, PDO::PARAM_INT]
        ], 'name', 'ASC');
    }

    /**
     * Fetch all players of the specified room who had answered the specified answer type
     * @param int $idRoom id of the room
     * @param EnumAnswer $answerType answer type
     * @return Player[] players ordred by name
     */
    public static function fetchAllOfRoomAndAnswer($idRoom, $answerType)
    {
        return self::fetchAll([
            ['id_room', $idRoom, PDO::PARAM_INT],
            ['current_answer', $answerType, PDO::PARAM_STR],
        ]);
    }
}
