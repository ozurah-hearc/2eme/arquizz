<?php

/**
 * The user class (information about a user).
 * The structure of the attributes is the same as the DB attribute, for easily (de)serialization.
 */
class User extends Model
{
    // --------------- Attributes --------------- //

    protected static $CLASS_NAME = 'User'; //used in Model
    protected static $PK_ID_NAME = "id_user"; //used in Model
    protected static $TABLE_NAME = 'User'; //used in Model

    protected $rooms = []; // Rooms object aggregation
    protected $quizzes = []; // Quizzs object aggregation

    // --------------- DB Attributes --------------- //

    protected $id_user = -1;

    protected $password = "";
    protected $email = "";

    // ----------- Getters and Setters (DB sources) ---------- //

    /**
     * Get the id of the user.
     * @return int ID of the user.
     */
    public function getId()
    {
        return $this->id_user;
    }

    /**
     * Set the id of the user.
     * @param int $value new id of the user.
     */
    public function setId($value)
    {
        $this->id_user = $value;
    }

    /**
     * Get the hashed password of the user.
     * @return string hashed password of the user.
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the hashed password of the user.
     * @param string $value new hashed password of the user.
     */
    public function setPassword($value)
    {
        $this->password = $value;
    }

    public function setAndHashPassword($unhashedPassword)
    {
        $this->setPassword(
            password_hash($unhashedPassword, PASSWORD_DEFAULT)
        );
    }

    /**
     * Get the email of the user.
     * @return string email of the user.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the email of the user.
     * @param string $value new email of the user.
     */
    public function setEmail($value)
    {
        $this->email = $value;
    }

    // ----------- Getters and Setters (not DB sources) ---------- //
    /**
     * 
     * Get the rooms of the user.
     * (this value isn't get from the DB, it should be setted before)
     * @return Room[] rooms of the user.
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set the rooms of the user.
     * @param Room[] $value new rooms of the user.
     */
    public function setRooms($value)
    {
        $this->rooms = $value;
    }

    /**
     * Get the quizzes of the user.
     * (this value isn't get from the DB, it should be setted before)
     * @return Quizz[] quizzes of the user.
     */
    public function getQuizzes()
    {
        return $this->quizzes;
    }

    /**
     * Set the quizzes of the user.
     * @param Quizz[] $value new quizzes of the user.
     */
    public function setQuizzes($value)
    {
        $this->quizzes = $value;
    }

    // --------------- Constructor -------------- //

    public function __construct()
    {
    }

    // --------------- Public DB Method ------------ //

    /**
     * Check if the user is authorized to authenticate with this password.
     * @param string $password hashed + salted password to try connection.
     * @return bool is the authenticate success ?
     */
    public function authenticate($password)
    {
        $user = parent::fetch([["email", $this->getEmail()]]);

        if (!$user) { //User not in database
            return false;
        }

        $this->setId($user->getId());
        $this->setEmail($user->getEmail());
        $this->setPassword($user->getPassword());

        // True or false depending if the password is valid or not
        return password_verify($password, $this->getPassword());
    }

    public function createAccount()
    {
        $values = [
            ['email', $this->getEmail(), PDO::PARAM_STR],
            ['password', $this->getPassword(), PDO::PARAM_STR],
        ];

        $filters = [];

        list($success, $lastInsertId) = parent::create($values, $filters, true);

        if ($success) {
            $this->setId($lastInsertId);
        }

        return $success;
    }
}
