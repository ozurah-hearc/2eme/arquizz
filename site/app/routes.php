<?php

$router->define([
    // ''                   => 'controllers/index.php',  // by conventions all controllers are in 'controllers' folder
    
    // Misc
    ''                          => 'IndexController'                                ,
    'index'                     => 'IndexController'                                ,
    'reactivate-event-db'       => 'IndexController'        . '@activateEventDB'    ,
    'about'                     => 'AboutController'                                ,
    
    // Connection
    'connection'                => 'ConnectionController'                           ,
    'connect'                   => 'ConnectionController'   . '@connect'            ,
    'disconnect'                => 'ConnectionController'   . '@disconnect'         ,
    'show-create-account'       => 'ConnectionController'   . '@showCreateAccount'  ,
    'create-account'            => 'ConnectionController'   . '@createAccount'      ,
    
    //Room
    'show-join-room'            => 'RoomController'         . '@showJoinRoom'       ,
    'join-room'                 => 'RoomController'         . '@joinRoom'           ,
    'room'                      => 'RoomController'                                 ,
    'show-create-room'          => 'RoomController'         . '@showCreateRoom'     ,
    'create-room'               => 'RoomController'         . '@createRoom'         ,
    'new-room'                  => 'RoomController'         . '@newRoom'            ,
    'start-room'                => 'RoomController'         . '@startRoom'          ,
    'stop-room'                 => 'RoomController'         . '@stopRoom'           ,
    'dynamic-parse-player'      => 'RoomController'         . '@continousParsePlayer',
    'dynamic-parse-question'    => 'RoomController'         . '@continousParseQuestion',
    'dynamic-noparse-answer'    => 'RoomController'         . '@noparseAnswer'      ,
    'dynamic-parse-roomState'   => 'RoomController'         . '@continousParseRoomState',
    'dynamic-parse-leaderboard' => 'RoomController'         . '@continousParseLeaderboard',
    
    //Quizz
    'show-quizzes'              => 'QuizzController'                                ,
    'show-quizz'                => 'QuizzController'        . '@showQuizz'          ,
    'edit-quizz'                => 'QuizzController'        . '@editQuizz'
]);
