<?php
$title = "Room management";
require('partials/header.php');
$code = htmlentities($room->getCode());
?>

<h1>Room</h1>

<?php if ($information["warningMsgAlreadyJoin"][0]) : ?>
	<p class="warning">
        <?= htmlentities($information["warningMsgAlreadyJoin"][1]) ?><br>
        You can't change your pseudo.<br>
        You join this room with your previous pseudo.
    </p>
<?php endif; ?>

<p>Room code <strong><?= $code ?></strong></p>

<p id="timerElement" class="hidden">Remaining time <span id="timerSec"><span id="timer">0</span>s</span></p>

<div id="viewSynchronisation" class="hidden">
    <p>This room is under synchronisation, please wait a few seconds before the next content</p>
</div>

<div id="viewWaiting" class="hidden">
    <p>This room is waiting to start</p>
</div>


<div id="viewQuestion" class="hidden">
    <p id="question-sentence">...</p>
    <ul id="answers">
        <li><button id="answer-a" class="answer" data-type='A' type="button">...</button></li>
        <li><button id="answer-b" class="answer" data-type='B' type="button">...</button></li>
        <li><button id="answer-c" class="answer" data-type='C' type="button">...</button></li>
        <li><button id="answer-d" class="answer" data-type='D' type="button">...</button></li>
    </ul>
</div>

<div id="viewLeaderboard" class="hidden">

    <p>The correct answer was <span id="correct-answer-type"><!-- [A, B, C, D] --></span>: <span id="correct-answer-sentence"><!-- [Some text] --></span></p>
    <p>You answered <span id="your-answer-type"><!-- [A, B, C, D] --></span>: <span id="your-answer-sentence">unknown<!-- [Some text] --></span></p>

    <h2 id="leaderboard-title-current">Current leaderboard</h2>
    <h2 id="leaderboard-title-finished" class="hidden">Final leaderboard</h2>

    <li id="leaderboard-item-template" class="hidden">
        <div class="leaderboard-item-rank"></div>
        <div class="leaderboard-item-name"></div>
        <div class="leaderboard-item-score"></div>
        <div class="leaderboard-item-score-earn"></div>
        <div class="leaderboard-item-score-final"></div>
    </li>

    <ul id="leaderboard-list" class="table">
    <li>
            <div>Rank</div>
            <div>Pseudo</div>
            <div>Previous Score</div>
            <div>Points earned</div>
            <div>Final Score</div>
        </li>
    </ul>
</div>

<script>
    'use strict'

    let remainingTime = 10;
    let state = "<?= EnumRoomState::Waiting ?>";

    let myScore = 0;

    const viewRemainingTime = document.getElementById('timer');
    const viewRemainingTimeElement = document.getElementById('timerElement');
    const viewSync = document.getElementById('viewSynchronisation');
    const viewWaiting = document.getElementById('viewWaiting');
    const viewQuestion = document.getElementById('viewQuestion');
    const viewLeaderboard = document.getElementById('viewLeaderboard');

    // Question elements (qt = Question)
    const qtQuestionSentence = document.getElementById('question-sentence');
    const qtAnswerA = document.getElementById('answer-a');
    const qtAnswerB = document.getElementById('answer-b');
    const qtAnswerC = document.getElementById('answer-c');
    const qtAnswerD = document.getElementById('answer-d');

    qtAnswerA.addEventListener('click', sendAnswer);
    qtAnswerB.addEventListener('click', sendAnswer);
    qtAnswerC.addEventListener('click', sendAnswer);
    qtAnswerD.addEventListener('click', sendAnswer);

    // Leaderboard elements (lb = LeaderBoard)
    const lbList = document.getElementById('leaderboard-list');
    const lbTitleCurrent = document.getElementById('leaderboard-title-current');
    const lbTitleFinished = document.getElementById('leaderboard-title-finished');
    const lbAnswerType = document.getElementById('correct-answer-type');
    const lbAnswerSentence = document.getElementById('correct-answer-sentence');

    const lbYourAnswerType = document.getElementById('your-answer-type');
    const lbYourAnswerSentence = document.getElementById('your-answer-sentence');

    async function updateByRoomState() {

        const data = await fetch('dynamic-parse-roomState', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idRoom: "<?= $room->getId() ?>",
            })
        });

        const json = await data.json();

        for (let dataJson of json) {
            remainingTime = dataJson.remainingTime;
            state = dataJson.state;


            viewSync.classList.add("hidden");
            viewWaiting.classList.add("hidden");
            viewQuestion.classList.add("hidden");
            viewLeaderboard.classList.add("hidden");

            if (remainingTime <= 0 &&
                state != "<?= EnumRoomState::Waiting ?>" &&
                state != "<?= EnumRoomState::Finished ?>" &&
                state != "<?= EnumRoomState::None ?>") {
                viewSync.classList.remove("hidden");
                viewRemainingTimeElement.classList.add("hidden");
                setTimeout(updateByRoomState, 500);
            } else {
                switch (state) {
                    case "<?= EnumRoomState::Waiting ?>":
                        console.log("wait");
                        remainingTime = 0;
                        viewRemainingTimeElement.classList.add("hidden");
                        viewWaiting.classList.remove("hidden");
                        setTimeout(updateByRoomState, 500);
                        break;
                    case "<?= EnumRoomState::Question ?>":
                        console.log("Will load question");
                        viewQuestion.classList.remove("hidden");
                        viewRemainingTimeElement.classList.remove("hidden");
                        setTimeout(updateByRoomState, remainingTime * 1000);
                        updateQuestion();
                        break;
                    case "<?= EnumRoomState::Leaderboard ?>":
                        console.log("Will load leaderboard");
                        viewLeaderboard.classList.remove("hidden");
                        viewRemainingTimeElement.classList.remove("hidden");
                        setTimeout(updateByRoomState, remainingTime * 1000);
                        updateLeaderBoard();
                        break;
                    case "<?= EnumRoomState::Finished ?>":
                        console.log('will load finished leaderboard');
                        viewRemainingTimeElement.classList.add("hidden");
                        remainingTime = 0;
                        viewLeaderboard.classList.remove("hidden");
                        updateLeaderBoard(true);
                        break;
                    default:
                        console.log(`Sorry, unhandled state.`);
                }
            }
        }
    }

    async function updateQuestion() {

        lbYourAnswerType.innerText = "";
        lbYourAnswerSentence.innerText = "nothing";

        qtAnswerA.classList.remove('selected-answer');
        qtAnswerB.classList.remove('selected-answer');
        qtAnswerC.classList.remove('selected-answer');
        qtAnswerD.classList.remove('selected-answer');

        const data = await fetch('dynamic-parse-question', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idRoom: "<?= $room->getId() ?>",
                idPlayer: "<?= $player->getId() ?>",
                score: myScore,
            })
        });
        
        const json = await data.json();
        console.log(json);

        const answers = json.answers;

        qtQuestionSentence.innerText = json.question.sentence;
        qtAnswerA.innerText = answers.A.sentence;
        qtAnswerB.innerText = answers.B.sentence;
        qtAnswerC.innerText = answers.C.sentence;
        qtAnswerD.innerText = answers.D.sentence;
    }

    async function sendAnswer(ev) {

        const element = ev.currentTarget;

        const answerType = element.dataset.type;
        const answerSentence = element.innerText;

        console.log('Selected answer', answerType);
        
        const data = fetch('dynamic-noparse-answer', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idPlayer: "<?= $player->getId() ?>",
                answerType,
            })
        });

        qtAnswerA.classList.remove('selected-answer');
        qtAnswerB.classList.remove('selected-answer');
        qtAnswerC.classList.remove('selected-answer');
        qtAnswerD.classList.remove('selected-answer');

        element.classList.add('selected-answer');

        lbYourAnswerType.innerText = answerType;
        lbYourAnswerSentence.innerText = answerSentence;
        
    }

    async function updateRemainingTime() {
        if (remainingTime >= 0) {
            viewRemainingTime.innerText = remainingTime;
            remainingTime--;
        }
    }

    async function updateLeaderBoard(isFinished = false) {
        const data = await fetch('dynamic-parse-leaderboard', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idRoom: "<?= $room->getId() ?>",
                idPlayer: "<?= $room->getId() ?>",
            })
        });

        if (isFinished) {
            lbTitleFinished.classList.remove("hidden");
            lbTitleCurrent.classList.add("hidden");
        }

        //traiter le résult de data
        const json = await data.json();
        console.log("leaderboard return", json);



        for (let dataJson of json) {
            // Answer informations
            lbAnswerType.innerText = dataJson.answer.type;
            lbAnswerSentence.innerText = dataJson.answer.sentence;

            // Leaderboard informations
            //  - Sort the dictionary by values (source . https://www.educative.io/edpresso/how-can-we-sort-a-dictionary-by-value-in-javascript)
            var sortedPlayers = Object.keys(dataJson.players).map(
                (key) => {
                    return [key, dataJson.players[key]]
                });

            sortedPlayers.sort(
                (first, second) => {
                    return second[1].finalScore - first[1].finalScore;
                }
            );

            // - Clean current leaderboard

            //lbList.innerText = ""; // Not using this, because we want's to keep the header row

            // Using this solution to keep first child
            for (let li of lbList.querySelectorAll("li:not(:first-child)")) {
                li.remove()
            }

            // - Create the leaderboard content
            let rank = 0;
            let previousScore = -1;

            for (let player of sortedPlayers) {
                let playerId = player[0];
                let playerValue = player[1];

                // Create the line
                const lbItem = document.getElementById('leaderboard-item-template').cloneNode(true);
                lbItem.removeAttribute('id');
                lbItem.classList.remove("hidden");

                // Modify dynamic values
                if (previousScore != playerValue.finalScore) {
                    previousScore = playerValue.finalScore;
                    rank++;
                }

                let displayedName = playerValue.name;
                if (playerId == <?= $player->getId() ?>) {
                    displayedName += " (You)";
                    lbItem.classList.add("leaderboard-item-you");

                    myScore = playerValue.finalScore;
                }

                const rankItem = lbItem.querySelector('.leaderboard-item-rank');
                const rankSpan = document.createElement('span');
                rankSpan.classList.add('leaderboard-span-rank');
                rankItem.appendChild(rankSpan);

                if (rank == 1) {
                    rankItem.classList.add('trophy-gold');
                } else if (rank == 2) {
                    rankItem.classList.add('trophy-silver');
                } else if (rank == 3) {
                    rankItem.classList.add('trophy-bronze');
                }

                // Set values in the line
                rankSpan.innerText = rank;
                lbItem.querySelector('.leaderboard-item-name').innerText = displayedName;
                lbItem.querySelector('.leaderboard-item-score').innerText = playerValue.currentScore;
                lbItem.querySelector('.leaderboard-item-score-earn').innerText = playerValue.scoreEarned;
                lbItem.querySelector('.leaderboard-item-score-final').innerText = playerValue.finalScore;
                lbList.appendChild(lbItem);
            }
        }
    }

    setInterval(updateRemainingTime, 1000);
    updateRemainingTime();
    updateByRoomState();
</script>

<?php require('partials/footer.php') ?>