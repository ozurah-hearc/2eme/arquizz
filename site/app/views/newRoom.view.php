<?php
$title = "Room management";
require('partials/header.php');
$code = htmlentities($room->getCode());
?>

<h1>Room Management</h1>

<?php if ($information["errorUnknownAction"]) : ?>
    <p class="error">This action is unhandled</p>
<?php endif; ?>

<?php if ($information["errorCantStart"]) : ?>
    <p class="error">The room can't be started</p>
<?php endif; ?>

<?php if ($information["errorAlreadyFinished"]) : ?>
    <p class="warning">The room is already finished</p>
<?php endif; ?>

<?php if ($information["errorStateUpdate"]) : ?>
    <p class="error">The state of the room didn't change (error with the DB)</p>
<?php endif; ?>

<p class="center">Room code</p>
<h2 class="center"><?= $code ?></h2>


<?php if ($room->getState() == EnumRoomState::Waiting) : ?>
    <form method="post" action="start-room" id="form-start-room">
        <input type="hidden" name="code" value="<?= $code ?>">
        <button type="submit" class="button" name="action_start">Start room</button>
    </form>
    <p>Waiting for players</p>


<?php elseif ($room->getState() != EnumRoomState::Finished) : ?>
    <form method="post" action="stop-room" id="form-stop-room">
        <input type="hidden" name="code" value="<?= $code ?>">
        <button type="submit" class="button" name="action_stop">Stop room</button>
    </form>
    <p>Current state : <?= htmlentities($room->getState()) ?> </p>


<?php else : ?>
    <p>The room is finished, create a new one</p>
<?php endif; ?>

<p>Current players :</p>
<ul id="currentPlayers">
</ul>

<script>
    'use strict'

    let currentPlayersId = [];
    const playersArea = document.getElementById('currentPlayers');


    async function updatePlayers() {

        const data = await fetch('dynamic-parse-player', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idRoom: "<?= $room->getId() ?>",
                playersId: currentPlayersId
            })
        });
        
        const json = await data.json();

        for (let player of json) {
            const li = document.createElement("li");
            li.appendChild(document.createTextNode(player.name));
            playersArea.appendChild(li);

            currentPlayersId.push(player.id);
        }
    }

    <?php if ($room->getState() == EnumRoomState::Waiting) : ?>
        setInterval(updatePlayers, 5000);
    <?php endif; ?>
    updatePlayers();
    
</script>

<?php require('partials/footer.php') ?>