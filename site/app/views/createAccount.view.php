<?php
$title = "Connection";
require('partials/header.php')
?>
<h1>Account Creation</h1>

<?php if ($information["errorInvalid"]) : ?>
	<p class="error">Invalid email or password</p>
<?php endif; ?>
<?php if ($information["errorPasswordNotMatch"]) : ?>
	<p class="error">Passwords didn't matches</p>
<?php endif; ?>
<?php if ($information["errorAlreadyExists"]) : ?>
	<p class="error">This email already exists, please connect with it</p>
<?php endif; ?>

<form action="create-account" method="post">

	<div class="input-row">
		<label for="email">Email</label>
		<input id="email" type="email" name="email" maxlength="100" required autofocus>
	</div>
	<div class="input-row">
		<label for="password">Password</label>
		<input id="password" type="password" name="password" maxlength="100" required>
	</div>
	<div class="input-row">
		<label for="passwordVerification">Retype your password</label>
		<input id="passwordVerification" type="password" name="passwordVerification" maxlength="100" required>
	</div>
	<div class="input-row">
		<input type="submit" class="button" value="Submit">
	</div>
	<p class="warning">
		&#9888;&#65039; In this version of the site, there is no email verification, that means a forgotten password is a lost account ! &#9888;&#65039;
	</p>
</form>

<?php require('partials/footer.php') ?>