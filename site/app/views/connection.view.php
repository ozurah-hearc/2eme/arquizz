<?php
$title = "Connection";
require('partials/header.php')
?>
<h1>Connection</h1>

<?php if ($information["errorInvalid"]) : ?>
	<p class="error">Invalid email or password</p>
<?php endif; ?>

<form action="show-create-account" method="post">
	<div class="input-row">
		<label for="noAccountRedirection">No account ?</label>
		<input type="submit" id="noAccountRedirection" class="button" value="Create one !" />
	</div>
</form>


<form action="connect" method="post">
	<div class="input-row">
		<label for="email">Email</label>
		<input id="email" type="email" name="email" maxlength="100" required autofocus>
	</div>
	<div class="input-row">
		<label for="password">Password</label>
		<input id="password" type="password" name="password" maxlength="100" required>
	</div>
	<div class="input-row">
		<input type="submit" class="button" value="Submit">
	</div>
</form>

<?php require('partials/footer.php') ?>