<?php
$title = "Join room";
require('partials/header.php');
?>

<?php if ($information["errorMsgNotExist"][0]) : ?>
	<p class="error"><?= htmlentities($information["errorMsgNotExist"][1]) ?></p>
<?php endif; ?>

<?php if ($information["errorMsgAlreadyStarted"][0]) : ?>
	<p class="error"><?= htmlentities($information["errorMsgAlreadyStarted"][1]) ?></p>
<?php endif; ?>

<?php if ($information["errorJoin"] || $information["errorPlayerId"]) : ?>
    <p class="error">An error occured, please retry</p>
<?php endif; ?>

<?php if ($information["errorPlayerId"]) : ?>
    <p class="error">You have never joined this room, use the form below</p>
<?php endif; ?>

<?php if ($information["errorUnknownRoom"]) : ?>
    <p class="error">The specified room doesn't exists</p>
<?php endif; ?>

<h1>Join room</h1>

<form method="post" action="join-room" id="form-join-room">
    <div class="input-row">
        <label for="room-code">Room code</label>
        <input id="room-code" type="text" name="room_code" required>
    </div>
    <div class="input-row">
        <label for="player-name">Player name</label>
        <input id="player-name" type="text" name="player_name" maxlength="20" required>
    </div>
    <button type="submit" class="button">Join room</button>
</form>

<?php require('partials/footer.php') ?>
