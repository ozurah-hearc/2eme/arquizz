<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">

	<meta name="author" content="Chappuis Sébastien & Allemann Jonas">
	<meta name="description" content="Web project Arquizz - A personnal kahoot
	   - Web Applications II - Course 2254.1
	   - Software development orientation (ISC2il-a)
       - HE-Arc - Computer science">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

	<title><?= htmlentities($title) ?></title>
	<link rel="stylesheet" href="public/style.css">
	<link rel="icon" type="image/x-icon" href="public/image/icon-colors.png">
</head>

<body>
	<header>
		<nav>
			<a href="/<?= Helper::getInstallPrefix() ?>" class="nav-icon icon-home">Home</a>
			<?php if (isset($_SESSION['id_user'])) : ?>
				<a href="show-quizzes" class="nav-icon icon-quizzes">Quizzes list</a>
				<a href="show-create-room" class="nav-icon icon-rooms">My rooms</a>
			<?php endif; ?>
			<a href="show-join-room" class="nav-icon icon-join-room">Join room</a>
			<a href="about" class="nav-icon icon-about">About</a>
			<?php if (isset($_SESSION['id_user'])) : ?>
				<a href="disconnect" class="nav-icon icon-disconnect">Disconnect</a>
			<?php else : ?>
				<a href="connection" class="nav-icon icon-connect">Connection</a>
				<a href="show-create-account" class="nav-icon icon-create-account">Create Account</a>
			<?php endif; ?>
		</nav>
	</header>
	<div class="wrapper">
		<main>