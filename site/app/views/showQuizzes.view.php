<?php
$title = "Quizzes list";
require('partials/header.php');
$errorFinaliseStatus = $information["errorFinaliseDb"] ? "(when applying all modifications on the db)" : "";
?>

<h1>Quizzes list</h1>

<?php if ($information["errorNotExists"] || $information["errorNotBelong"]) : ?>
	<p class="error">The specified quizz cannot be edited</p>
<?php endif; ?>

<?php if ($information["errorUnknownAction"]) : ?>
	<p class="error">This action is unhandled</p>
<?php endif; ?>

<?php
if ($information["statusDeleted"][0]) :
	[$class, $msg] = $information["statusDeleted"][1] ? ["success", "is done"] : ["error", "didn't work"];
?>
	<p class="<?= $class ?>">The deletion <?= $msg ?> <?= $errorFinaliseStatus ?></p>
<?php endif; ?>

<?php
if ($information["statusUpdated"][0]) :
	[$class, $msg] = $information["statusUpdated"][1] ? ["success", "is done"] : ["error", "didn't work"];
?>
	<p class="<?= $class ?>">The update <?= $msg ?> <?= $errorFinaliseStatus ?></p>
<?php endif; ?>

<?php
if ($information["statusCreated"][0]) :
	[$class, $msg] = $information["statusCreated"][1] ? ["success", "is done"] : ["error", "didn't work"];
?>
	<p class="<?= $class ?>">The creation <?= $msg ?> <?= $errorFinaliseStatus ?></p>
<?php endif; ?>

<a class="button" href="show-quizz">Create a new quizz</a>

<ul class="table">
	<li>
		<div class="hidden">Id</div>
		<div>Quizz</div>
		<div>Number of questions</div>
		<div>Owner</div>
	</li>

	<?php foreach ($quizzes as $quizz) : ?>
		<li>
			<div class="hidden"><?= htmlentities($quizz->getId()) ?></div>
			<div><?= htmlentities($quizz->getName()) ?></div>
			<div><?= count($quizz->getQuestions()) // not using countQuestion() to avoid useless access to the db
					?></div>
			<div><?= htmlentities($quizzOwners[$quizz->getId()]) ?></div>
		</li>
	<?php endforeach; ?>
</ul>

<script>
	const rows = Array.from(document.querySelectorAll('.table li'));

	rows.shift(); // Remove the first row, which is the header

	for (let row of rows) {
		row.addEventListener('click', onClickQuizz);
	}

	function onClickQuizz() {
		const cells = this.children;
		const quizzId = cells[0].innerText;
		window.location.href = "show-quizz?id=" + quizzId;
	}
</script>

<?php require('partials/footer.php') ?>