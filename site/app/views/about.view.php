<?php
$title = "About ARQuizz";
require('partials/header.php')
?>

<img src="public/image/logo-colors.png" alt="logo" class="logo">

<h1>About ARQuizz</h1>

<h2>Why this project ?</h2>
<p>
    ARQuizz is a project used to the evaluation of the course.<br>
    This project is used to practice the creation of a full and operational website, with the framework studies during the formation.
</p>

<h2>What can I do on this site ?</h2>
<h3>For everyone</h3>
<p>
    On this site you can play quizzes with other peoples by joining rooms.<br>
    A quiz game is separated into 2 phases (question and leaderboard) which alternate until the end of the quizz.<br>
    <br>
    During a question, just choose the correct answer. And during the leaderboard, see your ranking versus the other peoples who participate to this quiz.<br>
    Each player sees the same state of the game at the same moment, and each state has a delay before the next one.
</p>

<h3>For connected people</h3>
<p>
    You can create quizzes and rooms, you can also edit or delete your quizzes.
</p>

<h2>Credits</h2>
<p>
    Authors of the site: Chappuis Sébastien & Allemann Jonas (ISC2il-a)<br>
    Domain: HE-Arc - Computer science, software development orientation - 2nd year (2021-2022).<br>
    Course: Web Applications II - 2254.1<br>
    Images, icons are home made<br>
    Inspiration source : <a href="https://kahoot.it/">Kahoot</a>
</p>

<?php require('partials/footer.php') ?>