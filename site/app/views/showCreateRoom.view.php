<?php
$title = "Room list";
require('partials/header.php');
?>

<?php if ($information["errorUnknownRoom"]) : ?>
    <p class="error">The specified room doesn't exists</p>
<?php endif; ?>

<?php if ($information["errorNotBelong"]) : ?>
    <p class="error">The wanted room doesn't not belong to your account</p>
<?php endif; ?>

<h1>Create new room</h1>

<?php if ($information["errorUnknownAction"]) : ?>
    <p class="error">This action is unhandled</p>
<?php endif; ?>

<?php if ($information["errorAlreadyExists"]) : ?>
    <p class="error">The specified code already exists, try another one</p>
<?php endif; ?>

<?php if ($information["errorMissingData"]) : ?>
    <p class="error">An input was missing or wrong format</p>
<?php endif; ?>

<?php if ($information["errorCreate"]) : ?>
    <p class="error">The creation of the room failed, please retry</p>
<?php endif; ?>



<form method="post" action="create-room" id="form-quizz" class="form-large">

    <div class="input-row">
        <label for="roomCode">Code for join room</label>
        <input id="roomCode" class="input-all-width" type="text" name="roomCode" placeholder="[optional, blank to generate one]" maxlength="15">
    </div>

    <div class="input-row">
        <label for="quizzList">Choose a quizz</label>
        <select id="quizzList" class="input-all-width" name="quizzList">
            <?php foreach ($quizzes as $quizz) : ?>
                <option value="<?= $quizz->getId() ?>"><?= htmlentities($quizz->getName()) ?></option>
            <?php endforeach ?>
        </select>
    </div>
    
    <div class="input-row">
        <label for="roomCode">Quizz state time (in seconds)</label>
        <input id="time" type="number" class="input-all-width" name="time" min="15" step="15" placeholder="[optional, default <?= Room::DEFAULT_QUESTION_TIME ?>s]">
        <!-- Step 15 is because the auto changement state of the room is done every 15 seconds -->
    </div>

    <button type="submit" class="button" name="action_create">Create room</button>
</form>

<h1>My room list</h1>

<?php if (count($rooms) > 0) : ?>
    <ul class="table">
        <li>
            <div>Id</div>
            <div>Room code</div>
            <div>Room state</div>
            <div>Used quizz</div>
            <div>Current question</div>
            <div>Will be deleted after</div>
        </li>

        <?php foreach ($rooms as $room) :
        ?>
            <li>
                <div><?= htmlentities($room->getId()) ?></div>
                <div><?= htmlentities($room->getCode()) ?></div>
                <div><?= htmlentities($room->getState()) ?></div>
                <div><?= htmlentities($room->getQuizz()->getName()) ?></div>
                <div><?= htmlentities($room->getCurrentQuestion()) ?> / <?= htmlentities($room->getNbQuestion()) ?></div>
                <div><?= htmlentities($room->getAutoKill()->format('d-m-Y H:i:s')) ?></div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else : ?>
    <p>You haven't create a room now, try to create one !</p>
<?php endif; ?>

<script>
    const rows = Array.from(document.querySelectorAll('.table li'));

	rows.shift(); // Remove the first row, which is the header

    for (let row of rows) {
        row.addEventListener('click', onClickRoom);
    }

    function onClickRoom() {
        const cells = this.children;
        const roomCode = cells[1].innerText;
        window.location.href = "new-room?code=" + roomCode;
    }
</script>

<?php require('partials/footer.php') ?>