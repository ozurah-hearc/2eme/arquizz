<?php
$title = "Home";
require('partials/header.php')
?>

<img src="public/image/logo-colors.png" alt="logo" class="logo">

<h1>Home</h1>

<p>
    Welcome to this beautiful site that allows you to play quizzes with your friends.
</p>

<?php if (isset($_SESSION['id_user'])) : ?>
    <?php if (isset($_SESSION['email'])) : ?>
        Hello <?= htmlentities($_SESSION['email']) ?> !<br>
    <?php endif; ?>
    <p>You can go to the <a href="show-quizzes">quizz list</a> for creating (or editing) quizzes.</p>
    <p>If you want to create a room (or show your rooms), go to <a href="show-create-room">my rooms</a>.</p>
<?php endif; ?>

<p>Participate in a quizz by joining a room in <a href="show-join-room">Join room</a>.</p>

<?php if (!isset($_SESSION['id_user'])) : ?>
    <p>If you want to create quizzes or rooms, you need to be connected. You can do this on the <a href="connection">connection</a> page.</p>
<?php endif; ?>


<?php require('partials/footer.php') ?>