<?php
$title = "Quizz management";
require('partials/header.php');

?>
<h1>View and edit quizz</h1>

<form method="post" action="edit-quizz" id="form-questions" class="form-large">

    <div class="input-row">
        <label for="quizz-name">Quizz name</label>
        <input id="quizz-name" class="input-large" type="text" name="quizzName" placeholder="" value="<?= htmlentities($quizz->getName()) ?>" maxlength="20">
    </div>

    <ul id="ul-questions">
        <!-- The actual quizz ID sent to the server for edition/deletion -->
        <input id="hidden-edit-quizz-id" type="hidden" value="<?= $quizz->getId() ?>" name="quizzId">

        <?php $i = 0;
        foreach ($questions as $question) : ?>
            <li>
                <h2>Question <span class="question-visual-index"><?= $i + 1 ?></span></h2>
                <input type="hidden" name="questions[]" value="E_<?= $question->getId() ?>">
                <input type="text" class="input-large input-all-width" name="question<?= $question->getId() ?>" placeholder="Question sentence here" value="<?= htmlentities($question->getSentence()) ?>" maxlength="100">
                <ul class="answers">
                    <?php $j = 0;
                    foreach ($question->getAnswers() as $answer) : ?>
                        <li>
                            <h3>Answer <?= "ABCD"[$j] ?></h3>
                            <div class="answer-inputs">
                                <input type="radio" name="correct<?= $question->getId() ?>" value="<?= $answer->getType() ?>" <?= $answer->getIsCorrect() ? 'checked' : '' ?>>
                                <input type="text" name="answers<?= $question->getId() ?>[]" placeholder="answer here" value="<?= htmlentities($answer->getSentence()) ?>" maxlength="100">
                            </div>
                        </li>
                    <?php $j++;
                    endforeach; ?>
                </ul>
                <div class="question-input-row">
                    <button type="button" class="button btn-delete-question">Delete question</button>
                    <button type="button" class="button btn-move-question-up">Move up</button>
                    <button type="button" class="button btn-move-question-down">Move down</button>
                </div>
            </li>
        <?php $i++;
        endforeach; ?>
    </ul>
    <template id="template-question">
        <h2>Question <span class="question-visual-index">%VISUAL_INDEX%</span></h2>
        <input type="hidden" name="questions[]" value="C_%QUESTION_ID%">
        <input class="input-large input-all-width" type="text" name="question%QUESTION_ID%" placeholder="question sentence here" maxlength="100">
        <ul class="answers">
            <?php for ($i = 0; $i < 4; $i++) : ?>
                <li>
                    <h3>Answer <?= "ABCD"[$i] ?></h3>
                    <div class="answer-inputs">
                        <input type="radio" name="correct%QUESTION_ID%" value="<?= "ABCD"[$i] ?>" <?= $i === 0 ? 'checked' : '' ?>>
                        <input type="text" name="answers%QUESTION_ID%[]" placeholder="answer here" maxlength="100">
                    </div>
                </li>
            <?php endfor; ?>
        </ul>
        <div class="question-input-row">
            <button type="button" class="button btn-delete-question">Delete question</button>
            <button type="button" class="button btn-move-question-up">Move up</button>
            <button type="button" class="button btn-move-question-down">Move down</button>
        </div>
    </template>
    <button type="button" id="new-question" class="button">Add new question</button>
    <hr>
	<div class="quizz-input-row">
		<?php // Content of the "Create quizz" button
		$createBtnText = ($quizz->getId() >= 0) ? "Duplicate quizz" : "Create quizz";
		?>
		<button type="submit" class="button" name="action_create"><?= $createBtnText ?></button>
		
		<?php if ($quizz->getId() >= 0 && $displayRW) : // Negative index should not be possible, that means it's default
		?>
			<button type="submit" class="button" name="action_update">Update quizz</button>
			<button type="submit" class="button" name="action_delete">Delete quizz</button>
		<?php endif; ?>
	</div>
</form>

<script>
    (function() {
        'use strict';

        const formQuestions = document.getElementById('form-questions');
        const ulQuestions = document.getElementById('ul-questions');
        const templateQuestion = document.getElementById('template-question');
        const btnNewQuestion = document.getElementById('new-question');

        const btnListDeleteQuestion = document.getElementsByClassName('btn-delete-question');
        const btnListMoveQuestionUp = document.getElementsByClassName('btn-move-question-up');
        const btnListMoveQuestionDown = document.getElementsByClassName('btn-move-question-down');

        for (let btn of btnListDeleteQuestion) {
            btn.addEventListener('click', deleteQuestion);
        }
        for (let btn of btnListMoveQuestionUp) {
            btn.addEventListener('click', moveQuestionUp);
        }
        for (let btn of btnListMoveQuestionDown) {
            btn.addEventListener('click', moveQuestionDown);
        }

        let idCounter = 7001;

        btnNewQuestion.addEventListener('click', createQuestion);

        function createQuestion(ev) {

            let html = templateQuestion.innerHTML;

            const visualIndex = ulQuestions.children.length;

            html = html.replace(/%VISUAL_INDEX%/g, visualIndex);
            html = html.replace(/%QUESTION_ID%/g, idCounter);
            idCounter++;

            const li = document.createElement('li');
            li.innerHTML = html;

            const btnDeleteQuestion = li.getElementsByClassName('btn-delete-question')[0];
            const btnMoveQuestionUp = li.getElementsByClassName('btn-move-question-up')[0];
            const btnMoveQuestionDown = li.getElementsByClassName('btn-move-question-down')[0];

            btnDeleteQuestion.addEventListener('click', deleteQuestion);
            btnMoveQuestionUp.addEventListener('click', moveQuestionUp);
            btnMoveQuestionDown.addEventListener('click', moveQuestionDown);

            ulQuestions.appendChild(li);
        }

        function deleteQuestion(ev) {

            const btn = ev.currentTarget;

            let li = btn;
            while (li.tagName !== 'LI') li = li.parentElement;

            const questionHiddenInput = li.querySelector('[name="questions[]"]');
            const questionPrefix = questionHiddenInput.value[0]; // ex: 'C' or 'E'
            const questionId = questionHiddenInput.value.slice(2); // ex: 'C_123' --> '123'

            if (questionPrefix === 'E') {
                const input = document.createElement('input');
                input.type = 'hidden';
                input.name = 'deleted_questions[]';
                input.value = questionId;
                formQuestions.appendChild(input);
            }

            ulQuestions.removeChild(li);

            recalcVisualIndexes();
        }

        function moveQuestionUp(ev) {

            const btn = ev.currentTarget;

            let li = btn;
            while (li.tagName !== 'LI') li = li.parentElement;

            const prevLi = li.previousElementSibling;
            if (prevLi === null) return;

            swapQuestions(li, prevLi);

            li.scrollIntoView();
        }

        function moveQuestionDown(ev) {

            const btn = ev.currentTarget;

            let li = btn;
            while (li.tagName !== 'LI') li = li.parentElement;

            const nextLi = li.nextElementSibling;
            if (nextLi === null) return;

            swapQuestions(nextLi, li);

            li.scrollIntoView();
        }

        function swapQuestions(li1, li2) {

            ulQuestions.insertBefore(li1, li2);

            const span1 = li1.getElementsByClassName('question-visual-index')[0];
            const span2 = li2.getElementsByClassName('question-visual-index')[0];

            const id1 = span1.innerText;
            const id2 = span2.innerText;

            span1.innerText = id2;
            span2.innerText = id1;
        }

        function recalcVisualIndexes() {

            let index = 1;
            const spans = ulQuestions.getElementsByClassName('question-visual-index');

            for (let span of spans) {
                span.innerText = index;
                index++;
            }
        }

    })();
</script>

<?php require('partials/footer.php') ?>