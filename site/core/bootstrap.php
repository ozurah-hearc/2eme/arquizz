<?php

require 'vendor/autoload.php';
require 'core/Router.php';
require 'core/Request.php';
require 'core/Log.php';
require 'core/Enum.php';
require 'core/App.php';
require 'core/database/Connection.php';
require 'core/database/Model.php';
require 'helpers/Helper.php';

$lifetime = 60 * 60 * 24 * 7; // time in seconds : -> 1 week
session_start();
setcookie(session_name(), session_id(), time() + $lifetime); //store the session to a cookie for beeing
//                                                         able to be "auto relog" to the site when
//                                                         browser is closed
// use this way instead of cookies to avoid duplicate value (in $_SESSION and $_COOKIE)

App::loadConfig("config.php");

App::set('dbh', Connection::make(App::get('config')['database']));
