<?php

/**
 * Core class which contains generic CRUD operation to manage the database
 */
abstract class Model
{
    // --------------- Attributes --------------- //

    /**
     * Name of the table where the request will be done (... FROM $TABLE_NAME ...).
     */
    protected static $TABLE_NAME = "unsetted";

    /**
     * Name of the primary key of the table. Must be an integer.
     */
    protected static $PK_ID_NAME = "id";

    /**
     * Name of the class, for the log system.
     */
    protected static $CLASS_NAME = "Model";

    // ----------- Getters ---------- //

    abstract protected function getId();

    public static function getTableName()
    {
        return static::$TABLE_NAME;
    }

    public static function getClassName()
    {
        return static::$CLASS_NAME;
    }

    // --------------- Public Method ------------ //

    /**
     * Get the database object.
     * @return PDO database ready to use
     */
    public static function getDB()
    {
        return App::get('dbh');
    }

    // --------------- Public DB Method ------------ //

    /**
     * This method will activate the event manager of the database.
     * Call this if the site seems to not automatically change is state,
     *  like the room alternation or the auto-deletion of elements
     */
    public static function activateEventDB()
    {
        $dbh = self::getDB();
        $req = "SET GLOBAL event_scheduler=\"ON\"";
        $statement = $dbh->prepare($req);
        $statement->execute();
    }

    /**
     * Perform the "CREATE" operation to the DB.
     * @param array $values values to update.
     * * The structure is [["table attribute", "value", PDO::TYPE], ...].
     * * @exemple ['description', "text", PDO::PARAM_STR].
     * @param bool $includesUserInLog is the user id will be displayed in the log.
     * @return array [0] (bool) did the insert work correctly ? [1] (int) last instert id.
     * If [0] is false, don't considere the content of [1]
     */
    public function create($values, $includesUserInLog = false)
    {
        $dbh = self::getDB();
        $tableName = static::$TABLE_NAME;

        // Creation of the core of the request
        $attrReqPrepare = "(";
        $valReqPrepare = "(";
        $previousSeparator = "";

        foreach ($values as $val) {
            $attrReqPrepare .= $previousSeparator . $val[0];
            $valReqPrepare .= $previousSeparator . "?";

            $previousSeparator = ", ";
        }
        $attrReqPrepare .= ")";
        $valReqPrepare .= ")";

        $req = "INSERT INTO $tableName $attrReqPrepare VALUES $valReqPrepare";

        //Aplication du contenu de la requete
        $statement = $dbh->prepare($req);

        for ($i = 0; $i < count($values); $i++) {
            $val = $values[$i];
            $statement->bindParam($i + 1, $val[1], $val[2]);
        }

        //Execution
        $lastInsertId = 0;
        $success = false;
        try {
            $success = $statement->execute();
        } catch (PDOException $Exception) {
            // Nothing to do
            $forBreakpoints = $Exception;
        }

        if ($success) {
            $lastInsertId = $dbh->lastInsertId(); // use the var instead recall pdo->lastInsertId(), car the content will be clean after the first call
            Log::logCRUD(
                static::$CLASS_NAME,
                $lastInsertId,
                'C',
                $includesUserInLog ? ($_SESSION['id_user'] ?? "undefined") : null,
                $includesUserInLog ? ($_SESSION['email'] ?? "") : null
            );
        }

        return [$success, $lastInsertId];
    }

    /**
     * Perform the "UPDATE" operation to the DB.
     * 
     * @param array $values values to update.
     * * The structure is [["table attribute", "value", PDO::TYPE], ...].
     * * @exemple ['description', "text", PDO::PARAM_STR].
     * @param array $filters conditions for the WHERE close.
     * * each condition will be tested with equality operator + AND for each condition.
     * * The structure is : [["attribute", "value"], ["attribute", "value"], ...].
     * * @exemple [["hello", "world"], ["nice", "day"]]) will set the where close to "WHERE word = hello AND nice = day".
     * @param bool $includesUserInLog is the user id will be displayed in the log.
     * @return bool did the update work correctly ?
     */
    public function update($values, $filters, $includesUserInLog)
    {
        $dbh = self::getDB();
        $tableName = static::$TABLE_NAME;

        // prepared statement with question mark placeholders (marqueurs de positionnement)
        $req = "UPDATE $tableName SET ";

        $previousSeparator = "";

        for ($i = 0; $i < count($values); $i++) {
            $value = $values[$i];
            $attrName = $value[0];
            $req .= $previousSeparator . "$attrName = ?";

            $previousSeparator = ", ";
        }

        $req .= " WHERE 1 = 1";

        foreach ($filters as $filter) {
            $attrName = $filter[0];
            $req .= " AND $attrName = ?";
        }

        $paramIndex = 1;

        $statement = $dbh->prepare($req);

        foreach ($values as $value) {
            $statement->bindParam($paramIndex, $value[1], $value[2]);
            $paramIndex++;
        }

        foreach ($filters as $filter) {
            $statement->bindParam($paramIndex, $filter[1], $filter[2]);
            $paramIndex++;
        }

        $success = false;
        try {
            $success = $statement->execute();
        } catch (PDOException $Exception) {
            // Nothing to do
            $forBreakpoints = $Exception;
        }

        if ($success) {
            Log::logCRUD(
                static::$CLASS_NAME,
                $this->getId(),
                'U',
                $includesUserInLog ? ($_SESSION['id_user'] ?? "undefined") : null,
                $includesUserInLog ? ($_SESSION['email'] ?? "") : null
            );
        }

        return $success;
    }

    /**
     * Perform the "DELETE" operation to the DB.
     * @param array $filters conditions for the WHERE close.
     * * each condition will be tested with equality operator + AND for each condition.
     * * The structure is : [["attribute", "value"], ["attribute", "value"], ...].
     * * @exemple [["hello", "world"], ["nice", "day"]]) will set the where close to "WHERE word = hello AND nice = day".
     * @param bool $includesUserInLog is the user id will be displayed in the log.
     * @return bool did the deletion work correctly ?
     */
    public function delete($filters, $includesUserInLog)
    {
        $dbh = self::getDB();
        $tableName = static::$TABLE_NAME;

        // prepared statement with question mark placeholders (marqueurs de positionnement)
        $req = "DELETE FROM $tableName WHERE 1 = 1";

        foreach ($filters as $filter) {
            $attrName = $filter[0];
            $req .= " AND $attrName = ?";
        }

        $statement = $dbh->prepare($req);

        for ($i = 0; $i < count($filters); $i++) {
            $filter = $filters[$i];
            $statement->bindParam($i + 1, $filter[1], $filter[2]); //$i+1 cause index start at 1
        }


        $success = false;
        try {
            $success = $statement->execute();
        } catch (PDOException $Exception) {
            // Nothing to do
            $forBreakpoints = $Exception;
        }

        if ($success) {
            Log::logCRUD(
                static::$CLASS_NAME,
                $this->getId(),
                'D',
                $includesUserInLog ? ($_SESSION['id_user'] ?? "undefined") : null,
                $includesUserInLog ? ($_SESSION['email'] ?? "") : null
            );
        }

        return $success;
    }

    // ----------- Public STATIC Method --------- //

    /**
     * Perform the "READ" operation for getting the serialized rows in the DB (all rows of the table).
     * @param array $filters conditions for the WHERE close.
     * * each condition will be tested with equality operator + AND for each condition.
     * * The structure is : [["attribute", "value", PDO::PARAM_TYPE], ["attribute", "value", PDO::PARAM_TYPE], ...].
     * * @exemple [["hello", "world", PDO::PARAM_STR], ["nice", "day", PDO::PARAM_STR]]) will set the where close to "WHERE word = hello AND nice = day".
     * @param string $orderBy attribute which will be used for the result order.
     * @param string $ascOrDesc "ASC" for ascending order; "DESC" for descending order.
     * @return object[] an array of serialized object, according the $CLASS_NAME type
     */
    public static function fetchAll($filters, $orderBy = null, $ascOrDesc = 'ASC')
    {
        $dbh = self::getDB();
        $tableName = static::$TABLE_NAME;

        $req = "SELECT * FROM $tableName WHERE 1 = 1"; //1 = 1 to avoid empty where clause

        foreach ($filters as $filter) {
            $attrName = $filter[0];
            $req .= " AND $attrName = ?";
        }

        if ($orderBy !== null) {
            $req .= " ORDER BY $orderBy $ascOrDesc";
        }

        $statement = $dbh->prepare($req);

        for ($i = 0; $i < count($filters); $i++) {
            $filter = $filters[$i];
            $statement->bindParam($i + 1, $filter[1], $filter[2]);
        }

        // TODO : NEED MORE SECURITY WITH A TRY CATCH ARROUND THE EXECUTE

        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS, static::$CLASS_NAME);
    }

    /**
     * Perform the "READ" operation for getting serialized row of the DB which match with the specified ID.
     * @param int $id id of the player to fetch.
     * @return object serialized object, according the $CLASS_NAME type and the $PK_ID_NAME, false if failed
     */
    public static function fetchId($id)
    {
        return self::fetchInt(static::$PK_ID_NAME, $id);
    }

    /**
     * Perform the "READ" operation for getting serialized row of the DB which match with the specified attribute.
     * @param string $attrName name of the attribute in the DB
     * @param int $id value of the wanted row.
     *          * ASSUMPTION : $value was validated by the caller.
     * @return object serialized object, according the $CLASS_NAME type, false if failed
     */
    public static function fetchInt($attrName, $value)
    {
        return self::fetch([[$attrName, $value, PDO::PARAM_INT]]);
    }

    /**
     * Perform the "READ" operation for getting a row of the DB.
     * @param array $filters conditions for the WHERE close.
     * * each condition will be tested with equality operator + AND for each condition.
     * * The structure is : [["attribute", "value", PDO::PARAM_TYPE], ["attribute", "value", PDO::PARAM_TYPE], ...].
     * * @exemple [["hello", "world", PDO::PARAM_STR], ["nice", "day", PDO::PARAM_STR]]) will set the where close to "WHERE word = hello AND nice = day".
     * @return object serialized object, according the $CLASS_NAME type, false if failed
     */
    public static function fetch($filters)
    {
        $dbh = self::getDB();
        $tableName = static::$TABLE_NAME;

        $req = "SELECT * FROM $tableName WHERE 1 = 1"; //1 = 1 to avoid empty where clause
        // Set the where close
        foreach ($filters as $filter) {
            $attrName = $filter[0];
            $req .= " AND $attrName = ?";
        }

        $statement = $dbh->prepare($req);
        $statement->setFetchMode(PDO::FETCH_CLASS, static::$CLASS_NAME);

        //bind the param of the where close
        for ($i = 0; $i < count($filters); $i++) {
            $filter = $filters[$i];
            $statement->bindParam($i + 1, $filter[1]);
        }

        // TODO : NEED MORE SECURITY WITH A TRY CATCH ARROUND THE EXECUTE

        $statement->execute();
        return $statement->fetch();
    }
}
