<?php

/**
 * Core class that establishes the connection with the DB.
 */
class Connection
{
    /**
     * Establish the connection with the DB.
     * @param array $config config of the DB to use.
     * @return PDO object to manipulate the DB.
     */
    public static function make($config)
    {
        try {
            $pdo = new PDO(
                $config['connection'] . ';port=' . $config['port'] . ';dbname=' . $config['dbname'],
                $config['username'],
                $config['password'],
                $config['options']
            );

            return $pdo;
        } catch (PDOException $e) {
            d($e);
            die('<br><br><b>Could not connect to the DB</b>');
        }
    }
}
