<?php
// We use PHP 7, in this version we havent a built-in enum system (enum are available in PHP 8.1) 
// Source of the stucture of enum + base class : https://stackoverflow.com/a/254543
//   (full post : https://stackoverflow.com/questions/254514/enumerations-on-php)

/**
 * An enum class for the type/order of answer
 */
abstract class EnumAnswer extends BasicEnum
{
    const None = 'None';
    const A = 'A';
    const B = 'B';
    const C = 'C';
    const D = 'D';
}

/**
 *  An enum class for the state of the room
 */
abstract class EnumRoomState extends BasicEnum
{
    const Question = 'Question';
    const Leaderboard = 'Leaderboard';
    const Finished = 'Finished';
    const Waiting = 'Waiting';
    const None = 'None';
}

/**
 * Add usefull methods for enum verification
 * More information here : https://stackoverflow.com/a/254543
 */
abstract class BasicEnum
{
    private static $constCacheArray = NULL;

    private static function getConstants()
    {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }
}
