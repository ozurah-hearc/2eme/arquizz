<?php

/**
 * Core class that contains the data of the app.
 */
class App
{
  /**
   * Dynamic registred data of the site.
   */
  private static $app = [];

  /**
   * Get a registred object.
   * @param object $k identifiant (key) of the registered object.
   * @return object value of the object.
   */
  public static function get($k)
  {
    return self::$app[$k];
  }

  /**
   * Add a new or set the value of registred object.
   * @param object $k identifiant (key) of the registered object.
   * @param object $v new value of the object.
   */
  public static function set($k, $v)
  {
    self::$app[$k] = $v;
  }

  /**
   * Load the config file of the site.
   * @param string $filename path and name of the file containing the config.
   */
  public static function loadConfig($fileName)
  {
    self::$app['config'] = require($fileName);
  }
}
