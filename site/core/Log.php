<?php

/**
 * Core class which allows to generate log file.
 */
class Log
{
	/**
	 * The path and name of the file where the log will be saved.
	 */
	private static $FILENAME = 'data.log';

	/**
	 * Add a new line to the file of a log.
	 * This method is optimized for a log after a CRUD operation.
	 * @param string $type type of the log (like the table where the CRUD is performed).
	 * @param int $id id of the element where the CRUD operation was done.
	 * @param string $operation CRUD Operation type indication (like C, R, U, D).
	 * @param int $userId id of the user which perform the crud operation.
	 * @param string $username name (or email) to humanly identify the user which perform the crud operation.
	 */
	public static function logCRUD($type, $id, $operation, $userId = null, $username = null)
	{
		$line = "type=$type, id=$id, <$operation>";

		if ($userId !== null) {
			$line .= " from user $userId";
		}

		if ($username !== null) {
			$line .= " ($username)";
		}

		self::logData($line);
	}

	/**
	 * Add a new line to the file of a log.
	 * This method is for generic log.
	 * @param string $type type of the log.
	 * @param string $message message to add to the log.
	 */
	public static function logMessage($type, $message)
	{
		$line = "type=$type, $message";

		self::logData($line);
	}

	/**
	 * Generic method for adding a line to the file of a log.
	 * Note : the current datetime is added at the start of the line.
	 * @param string $data data to save to the file.
	 */
	private static function logData($data)
	{
		$date = date("c");
		$line = "$date, $data";

		$line .= "\n";

		file_put_contents(self::$FILENAME, $line, FILE_APPEND);
	}
}
