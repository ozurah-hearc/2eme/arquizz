<?php

/**
 * Core class which contains some useful method about the site.
 */
class Request
{
  /**
   * Get the URI of the site.
   */
  public static function uri()
  {
    // trim: strip whitespace (or other characters) from the beginning
    // and end of a string
    return trim($_SERVER['REQUEST_URI'], '/');
  }
}
