<?php 
// How to run : [path to site folder]> ./vendor/bin/phpunit test

include "core/bootstrap.php";

use Symfony\Component\DomCrawler\Crawler;
use PHPUnit\Framework\TestCase;

final class HelperTest extends TestCase 
{
    public function testCanGetInstallPrefix(): void
    {
        $CONFIG = require "config.php";
        $this->assertEquals($CONFIG["install_prefix"], Helper::getInstallPrefix());
    }

    public function testIssetGetCleanSession(): void
    {
        $key = "test";
        $value = "yes";
        
        // Test with existing session variable
        $_SESSION[$key] = $value;
        $result = Helper::issetGetCleanSession($key);
        $this->assertTrue($result[0]);
        $this->assertEquals($value, $result[1]);

        // The session should be removed        
        $this->assertFalse(isset($_SESSION[$key]));

        // Test with unexisting session variable
        $result = Helper::issetGetCleanSession("unexisting");
        $this->assertFalse($result[0]);
        $this->assertNull($result[1]);
    }

    
    public function testDisplayView(): void
    {
        // Source pour tester du HTML : https://stackoverflow.com/questions/18643052/phpunit-asserting-html-content

        // Correctly load existing view
        $viewName = "about";
        
        $htmlRequire = Helper::view($viewName);
   
        ob_start(); // Convert an echo to variable
        echo $htmlRequire;
        $html = ob_get_clean();
        
    
        $crawler = new Crawler($html);
        $this->assertTrue((bool)$crawler->filterXPath('html')->count()); // The page successfully loaded
    }

    // Other tests is for redirection; kint display; 
}