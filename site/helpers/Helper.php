<?php

/**
 * Helper class which contains some useful method for any site.
 */
class Helper
{
    /**
     * Display the information about a variable (inside "pre" markup).
     * @param object $data variable to display.
     */
    public static function display($data)
    {
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        d($data);
    }

    /**
     * Display the view of a variable and die the site
     */
    public static function dd($data)
    {
        self::display($data);
        die();
    }

    /**
     * Dynamically require a view
     * @param string $name name of the view file (witout the .view.php part)
     * @param object[] $data data which can be used in the view
     * @return require of the HTML view page.
     */
    public static function view($name, $data = [])
    {
        extract($data); // La function extract importe les variables
        //                  dans la table des symboles
        //                  voir: http://php.net/manual/fr/function.extract.
        //                  voir aussi la méthode compact()

        return require "app/views/{$name}.view.php";
    }

    /**
     * Perform a redirect to the home page if the user isn't logged to the site
     */
    public static function goHomeIfNotLogged()
    {
        if (!isset($_SESSION['id_user'])) {
            self::goHome();
        }
    }


    /**
     * Perform a redirect to the home page
     */
    public static function goHome()
    {
        self::redirect("/" . self::getInstallPrefix());
    }

    /**
     * Perform a redirect to the specified address
     */
    public static function redirect($to)
    {
        header("Location: " . $to);
        exit();
    }

    /**
     * Get the install prefix of the site
     */
    public static function getInstallPrefix()
    {
        return App::get('config')['install_prefix'];
    }

    /**
     * Get the value of a variable stored and unset it from $_SESSION
     * @param string $var name of the attribue to get ($_SESSION[$var])
     * @return array
     *     [0] variable isset in the session
     *     [1] null if is not set; value of the variable if is set
     */
    public static function issetGetCleanSession($var)
    {
        $isset = isset($_SESSION[$var]);
        $val = isset($_SESSION[$var]) ? $_SESSION[$var] : null;
        unset($_SESSION[$var]);
        return [$isset, $val];
    }
}
